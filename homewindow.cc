#include <homewindow.h>
#include <data.h>

HomeWindow::HomeWindow():Window(0,0,-1,-1){

    mViewGroup = (ViewGroup *)LayoutInflater::from(getContext())->inflate("@layout/home_view",this);

    readModeData(this);                     //读取数据
    Activity_header::set_Parentview(mViewGroup);  // 设置顶部栏的 父指针，指向该窗口
    Activity_header::ins()->init();         // 初始化顶部栏

    mViewPager = (ViewPager *)findViewById(z10::R::id::page);       
    mTabLayout = (TabLayout*)findViewById(z10::R::id::tab);
    mAdapter   = new MyPageAdapter;
 
    mViewPager->setAdapter(mAdapter);                          //ViewPager 添加 PageAdapter
    mTabLayout->setupWithViewPager(mViewPager);                //TabLayout 与 page 绑定在一起

    notifyDataChanged();
    
    TabLayout::OnTabSelectedListener selectListener;
    selectListener.onTabSelected = std::bind(&HomeWindow::onTabSelectedListener,this,std::placeholders::_1);
    mTabLayout->addOnTabSelectedListener(selectListener);

    InputEventSource::getInstance().setScreenSaver(std::bind(&HomeWindow::screenSaver,this,std::placeholders::_1),60000);       //屏保

    mRunnable=[this](){
        LOGD("setClickable(true)");
        for (int i = 0; i < mTabLayout->getTabCount(); i++) {
            mTabLayout->getTabAt(i)->mView->setClickable(true);
            isCanSelect = true;
        }
    };
}   

void HomeWindow::onTabSelectedListener(TabLayout::Tab& tab){
    if(!isNotify){
        //判断上一个 ViewPager 页面是否有变化（温度、出水量）
        if(modeData.at(mPrevPagePosition).isChange){
            MyLinearLayout *linear = (MyLinearLayout *)modeData.at(mPrevPagePosition).myins;
            linear->resetPageData();
        }
        checkIsNeedPop(tab.getPosition());
        //每次选择了页面，先让 TabLayout 失能，不能点击，以免切换页面过快出现页面空白的情况
        for (int i = 0; i < mTabLayout->getTabCount(); i++) {
            mTabLayout->getTabAt(i)->mView->setClickable(false);
            isCanSelect = false;
        }
        removeCallbacks(mRunnable);
        postDelayed(mRunnable,500);
    }
    MyLinearLayout *linear = (MyLinearLayout *)modeData.at(mPrevPagePosition).myins;    // 设置下一个页面的 出水按钮
    linear->outWaterBtnStatus();
    removeAppCallback(SC_OUT_WATER_COMPLETE);
    addAppCallback(SC_OUT_WATER_COMPLETE,std::bind(&MyLinearLayout::outWaterDownCallback,linear));
    LOGI("onTabSelectedListener()   mPrevPagePosition = %d",mPrevPagePosition);
    LOG(VERBOSE) << "mPrevPagePosition =  " << mPrevPagePosition;
}

HomeWindow::~HomeWindow(){
   delete mViewPager;
   delete mTabLayout;
}

//屏保执行的函数 
void HomeWindow::screenSaver(bool bEnabled){
    if(bEnabled && !Page_Unlock::isPresent()){
        Screen_saver *wd = new Screen_saver;
    }
    
    const bool screenSaverActived= InputEventSource::getInstance().isScreenSaverActived();
    LOGD("screenSaver returned  actived=%d bEnabled=%d",screenSaverActived,bEnabled);
}

//更新 tablayout
void HomeWindow::notifyDataChanged(){
    isNotify = true;

    mAdapter->notifyDataChanged();
    mViewPager->setAdapter(mAdapter);                          //ViewPager 添加 PageAdapter

    resetPagePosition();
    // mViewPager->setCurrentItem(mPrevPagePosition);
    mTabLayout->getTabAt(mPrevPagePosition)->select();         //设置 TabLayou 的首选是 2
    LOGD("notifyDataChanged()    mPrevPagePosition =%d",mPrevPagePosition);

    // 重置 TabLayout 的默认属性，可自定义在 tabcustomview.xml 上修改样式
    if(mTabLayout != nullptr){
        for (int i = 0; i < mTabLayout->getTabCount(); i++) {
            TabLayout::Tab *t = mTabLayout->getTabAt(i);
            if (t != nullptr) {
                t->setCustomView(resetTabCustom(modeData.at(i).type,i));
            }   
        }
    }   
    isNotify = false;
}

//获取位置 ，不能是冰块或冷水
void HomeWindow::resetPagePosition(){
    for(int i =0;i < modeData.size();i++){
        if(modeData.at(i).type != MODE_TYPE_ICE && modeData.at(i).type != MODE_TYPE_COOL_WATER){
            mPrevPagePosition = i;
            LOGD("set mPrevPagePosition = %d ",mPrevPagePosition);
            return;
        }
    }
    mTabLayout->getTabAt(mPrevPagePosition)->select();
}

//切换到上一个页面
void HomeWindow::selectTab(){
    LOGD("selectTab  position = %d ",mPrevPagePosition);
    mTabLayout->getTabAt(mPrevPagePosition)->select();
}

//检查上一个页面是否 制冷或制冰
void HomeWindow::CheckSelect(){
    if((modeData.at(mPrevPagePosition).type == MODE_TYPE_ICE) && (g_appData.zhibing == PRO_STATE_ICE_NONE)){
        resetPagePosition();
    }else if((modeData.at(mPrevPagePosition).type == MODE_TYPE_COOL_WATER) && (g_appData.zhileng == PRO_STATE_COOL_NONE)){
        resetPagePosition();
    }
}

bool HomeWindow::checkIsNeedPop(int positon){
    LOG(VERBOSE) << "checkIsNeedPop  " << "  positon = " << positon;
    if(modeData.at(positon).type == MODE_TYPE_ICE){
        if(g_appData.zhibing == PRO_STATE_ICE_NONE){    //先判断 制冰 是否开启
            Popwindow_text_hint *window = new Popwindow_text_hint(mViewGroup,INPUT_FUNC_ICE);
            return true;
        }
        else if(g_appData.zhibing == PRO_STATE_ICE_ING){
            Popwindow_tip_hint *tip_window = new Popwindow_tip_hint(mViewGroup,INPUT_FUNC_ICE_ING);
            return true;
        }
        else if(g_appData.zhibing == PRO_STATE_ICE_DONE){
            mPrevPagePosition = positon;
        }
    }
    else if(modeData.at(positon).type == MODE_TYPE_COOL_WATER){
        if(g_appData.zhileng == PRO_STATE_COOL_NONE){                       //先判断 制冷 是否开启
            Popwindow_text_hint *window = new Popwindow_text_hint(mViewGroup,INPUT_FUNC_COOL);
            return true;
        }
        else if(g_appData.zhileng == PRO_STATE_COOL_ING){
            Popwindow_tip_hint *tip_window = new Popwindow_tip_hint(mViewGroup,INPUT_FUNC_COOL_ING);
            return true;
        }
        else if(g_appData.zhileng == PRO_STATE_COOL_DONE){
            mPrevPagePosition = positon;
        }
    }
    else{
        mPrevPagePosition = positon;
    }
    return false;
}   

View* HomeWindow::resetTabCustom(int type,int positon){
    View *customView;
    if(type == MODE_TYPE_ICE){
        customView= LayoutInflater::from(getContext())->inflate("@layout/tabcustomview_bingkuai", nullptr);
        customView->setOnTouchListener([this,type,positon](View& v, MotionEvent&event) {
            if((event.getAction()==MotionEvent::ACTION_UP)&& isCanSelect&& (!checkIsNeedPop(positon))) selectTab();
            return true;
        });
    }else if(type == MODE_TYPE_COOL_WATER){
        customView = LayoutInflater::from(getContext())->inflate("@layout/tabcustomview", nullptr);
        customView->setOnTouchListener([this,type,positon](View& v, MotionEvent&event) {
            LOGI("setOnTouchListener    MODE_TYPE_COOL_WATER  ");
            if((event.getAction()==MotionEvent::ACTION_UP)&& isCanSelect&& !checkIsNeedPop(positon)) selectTab();
            return true;
        });
    }else{
        customView = LayoutInflater::from(getContext())->inflate("@layout/tabcustomview", nullptr);
    }
    return customView;
}

void HomeWindow::removeAppCallback(appStatusChange status){
    auto it = mAppCallback.find(status);
    if (it != mAppCallback.end()) {
        mAppCallback.erase(it);
    }
}

void HomeWindow::addAppCallback(appStatusChange status, app_callback callback){
    auto it = mAppCallback.find(status);
    if (it == mAppCallback.end()) {
        mAppCallback.insert(std::make_pair(status, callback));
    }
}

void HomeWindow::AppCallback(appStatusChange status){
    auto it = mAppCallback.find(status);
    if (it != mAppCallback.end()) {
        it->second();
    }
}

// 程序状态更新
void HomeWindow::appStatusNotify(appStatusChange status,int value){     
    LOG(VERBOSE) << "status = " << status << "  value = " << value;
    
    // 报警响应
    switch (status) {
        case SC_WARN_INTAKE_WATER_PROBE: {
            LOGI("进水温探头报警...");
            break;
        }case SC_WARN_ICE_WATER_PROBE: {
            LOGI("冰水温探头报警...");
            break;
        }case SC_WARN_HOT_WATER_PROBE: {
            LOGI("热水温探头报警...");
            break;
        }case SC_WARN_OVER_DRY_HEATING: {
            LOGI("超温/干烧报警...");
            break;
        }case SC_WARN_MAKE_COOL: {
            LOGI("制冷异常...");
            break;
        }case SC_WARN_MAKE_HOT: {
            LOGI("制热异常...");
            break;
        }case SC_WARN_MAKE_ICE: {
            LOGI("结冰报警...");
            break;
        }case SC_WARN_STORTAGE_WATER: {
            LOGI("缺水报警...");
            break;
        }case SC_WARN_LEAKAGE_WATER: {
            LOGI("漏水报警...");
            break;
        }case SC_WARN_LVXIN: {
            LOGI("滤芯到期报警...");
            break;
        }case SC_WARN_FLOW: {
            LOGI("流量传感器报警...");
            break;
        }case SC_WARN_COMM_FAIL: {
            LOGI("通信故障(表示长时间未收到面板发往主板的信号)...");
            break;
        }
    }

    switch (status) {
        case SC_UV_COMPLETE: {
            LOGI("UV功能完成...");
            break;
        }case SC_ICE_COMPLETE: {
            LOGI("制冰完成(冰满)...");
            g_appData.zhibing = PRO_STATE_ICE_DONE;
            Activity_header::ins()->changeState(PRO_STATE_ICE_DONE);
            break;
        }case SC_WASH_COMPLETE: {
            LOGI("冲洗完成...");
            break;
        }case SC_COOL_WATER_TEM: {
            g_appData.iceWaterTem  = value;
            HomeWindow::ins()->AppCallback(SC_COOL_WATER_TEM);
            if((value > 12)&&(g_appData.zhileng == PRO_STATE_COOL_DONE)){
                LOGI("正在制冰中...");
                g_appData.zhileng = PRO_STATE_COOL_ING;
                Activity_header::ins()->changeState(PRO_STATE_COOL_ING);
            }else if((value >=6)&&(value <=12)&&(g_appData.zhileng == PRO_STATE_COOL_ING)){
                LOGI("制冷完成...");
                g_appData.zhileng = PRO_STATE_COOL_DONE;
                Activity_header::ins()->changeState(PRO_STATE_COOL_DONE);
            }    
            break;
        }case SC_OUT_WATER_COMPLETE: {
            LOGI("放水完成...");
            g_appData.outWaterStatus =  WA_NULL;
            g_appData.showStatusTDS  = PRO_STATE_TDS_HIDE;
            HomeWindow::ins()->AppCallback(SC_OUT_WATER_COMPLETE);
            break;
        }case SC_OUT_ICE_COMPLETE: {
            g_appData.iceStatus   =  false;
            LOGI("出冰块完成...");
            break;
        }case SC_PP_RESET_COMPLETE: {
            g_appData.isResetPP   =  false;
            g_appData.PPLvxinPercen = 0;
            status = SC_PP_PERCEN;
            value  = 0;
            LOGI("PP滤芯复位完成...");
            break;
        }case SC_RO_RESET_COMPLETE: {
            g_appData.isResetRO   =  false;
            g_appData.ROLvxinPercen = 0;
            status = SC_RO_PERCEN;
            LOGI("RO滤芯复位完成...");
            break;
        }case SC_TDS_STATE:{
            LOGI("TDS = %d...",value);
            g_appData.cleanWaterTDS = value;
            Activity_header::ins()->changeState((State)g_appData.showStatusTDS);
            break;
        }case SC_INWATER_TEM:{
            LOGI("进水温度、常温水温度变化 = %d...",value);
            g_appData.realWaterTem = value;
            HomeWindow::ins()->AppCallback(SC_INWATER_TEM);
            break;
        }
    }

    if((status == SC_PP_PERCEN )|| (status == SC_RO_PERCEN)){
        LOGI("滤芯 百分比更变 = %d...",value);
        if(status == SC_PP_PERCEN) g_appData.PPLvxinPercen = value;
        else                       g_appData.ROLvxinPercen = value;
        if(value < LVXIN_SON) 
            g_appData.lvxin = PRO_STATE_LVXIN_NORMAL;
        if(g_appData.PPLvxinPercen == LVXIN_WARN)
            g_appData.lvxin = PRO_STATE_LVXIN_EXPIRED;
        else if((g_appData.PPLvxinPercen >= LVXIN_SON)&&(g_appData.lvxin == PRO_STATE_LVXIN_NORMAL))
            g_appData.lvxin = PRO_STATE_LVXIN_SOON;
        
        if(g_appData.ROLvxinPercen == LVXIN_WARN)
            g_appData.lvxin = PRO_STATE_LVXIN_EXPIRED;
        else if((g_appData.ROLvxinPercen >= LVXIN_SON)&&(g_appData.lvxin == PRO_STATE_LVXIN_NORMAL))
            g_appData.lvxin = PRO_STATE_LVXIN_SOON;
        
        Activity_header::ins()->upHeader();
    }
}     

