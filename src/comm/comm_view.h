﻿
#ifndef __comm_view_h__
#define __comm_view_h__

#include "comm_func.h"
#include <cdroid.h>

////////////////////////////////////////////////////////////////
class IWindow : public Window {
public:
#define __get(I) (mVP)->findViewById(I)
#define __getv(T, I) (dynamic_cast<T *>((mVP)->findViewById(I)))
#define __getg(G, I) (G)->findViewById(I)
#define __getgv(G, T, I) (dynamic_cast<T *>((G)->findViewById(I)))

    IWindow();
    ~IWindow();

    bool       inflate(const std::string &resourece);
    ViewGroup *inflate(const std::string &resource, ViewGroup *root, bool attachToRoot = true);

    int64_t getTime() { return mLastTime; }

public:
    virtual std::string getClassName() { return "IWindow"; }

protected:
    LayoutInflater *mInflater; // 窗口xml加载器
    ViewGroup      *mVP;       // 窗口主容器
    int64_t         mLastTime; // 窗口最后活动时间
};

/////////////////////////////////////////////////////////////////////////
// 容器变化内容加载器
class ConnViewLoader {
protected:
#define __bind_cb(F) std::bind(F, this, std::placeholders::_1)
    typedef std::function<void(ViewGroup *)> conn_callback; // 浮窗回调接口
    typedef struct {
        std::string   xml;
        ViewGroup    *vg;
        conn_callback init;   // 初始化回调
        conn_callback active; // 激活回调
    } ConnItemData;

public:
    ConnViewLoader();
    ~ConnViewLoader();

protected:
    virtual LayoutInflater *getInflater()             = 0;
    virtual ViewGroup      *getConnParent(int connId) = 0;

    void regConn(int connId, const std::string &xml, conn_callback init, conn_callback active);
    void loadConn(int connId);

protected:
    int                         mConnId;
    std::vector<ViewGroup *>    mParents;
    std::map<int, ConnItemData> mMapConn;
};

///////////////////////////////////////////////////////////////////////////////////////
// 管理窗口 避免一个窗口重复创建
int      regWindow(int windowID, IWindow *windPtr);
IWindow *getWindow(int windowID);
void     checkWindow();
int      getWndCount();
int      getWindows(std::map<int, IWindow *> &wnds);
void     closeWindow(IWindow *windPtr);

// 组件状态
void setGroupEnabled(ViewGroup *vg, bool flag);
// 切换状态
void switchActivated(View *&oldv, View *newv);
// 移除并删除子控件
void removeAndDelAllViews(ViewGroup *vg);
// 隐藏视图
bool hideView(View *v, int flag = View::GONE);
// 显示视图
bool showView(View *v);
bool isShow(View *v);
// 转换为主窗口位置
void child2ParentRect(View *v, Rect &rc);
// 切换显示/隐藏状态
void changeView(View *v, int hide_flag = View::GONE);

// 获取滚动数据
typedef enum {
    NUMBER_PICKER_DT_YEAR = 1, // 年
    NUMBER_PICKER_DT_MONTH,    // 月
    NUMBER_PICKER_DT_DAY,      // 日
    NUMBER_PICKER_DT_HOUR_12,  // 12小时制
    NUMBER_PICKER_DT_HOUR_24,  // 24小时制
    NUMBER_PICKER_DT_MINUTE,   // 分钟
    NUMBER_PICKER_DT_DAY_PART, // 当天时段：上午，下午
    NUMBER_PICKER_DT_WEEK,     // 周
} NumberDataType;
void getNumberPickerData(NumberDataType t, std::vector<int> &val, std::vector<std::string> &txt, int lanId = 0);

#endif
