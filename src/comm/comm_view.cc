﻿
#include "comm_view.h"

#define WINDOW_RELEASE_TIME 10 // 窗口回收时间(秒)
#define WINDOW_MAX_COUNT 3     // 最多窗口数量

std::map<int, IWindow *> gMapWindows;
///////////////////////////////////////////////////////////////////////////
// IWindow class
IWindow::IWindow() : Window(0, 0, -1, -1) {
    mInflater = LayoutInflater::from(getContext());
    mLastTime = getTimeSec();
    mVP       = nullptr;
}

IWindow::~IWindow() {}

bool IWindow::inflate(const std::string &resourece) {
    mVP = __dc(ViewGroup, mInflater->inflate(resourece, this));
    return mVP != nullptr;
}

ViewGroup *IWindow::inflate(const std::string &resource, ViewGroup *root, bool attachToRoot /* = true */) {
    return __dc(ViewGroup, mInflater->inflate(resource, root, attachToRoot));
}

/////////////////////////////////////////////////////////////////////////
// 容器变化内容加载器
ConnViewLoader::ConnViewLoader() : mConnId(0) {}

ConnViewLoader::~ConnViewLoader() {
    for (ViewGroup *parent : mParents) { parent->removeAllViews(); }
    mParents.clear();
    for (auto kv : mMapConn) { __del(kv.second.vg); }
    mMapConn.clear();
}

void ConnViewLoader::regConn(int connId, const std::string &xml, conn_callback init, conn_callback active) {
    auto it = mMapConn.find(connId);
    if (it != mMapConn.end()) {
        LOGE("float already reg. id=%d xml=%s", connId, xml.c_str());
        return;
    }

    ConnItemData dat;
    dat.xml    = xml;
    dat.vg     = 0;
    dat.init   = init;
    dat.active = active;
    mMapConn.insert(std::make_pair(connId, dat));
}

void ConnViewLoader::loadConn(int connId) {
    ViewGroup *vg_parent = getConnParent(connId);

    if (vg_parent->getVisibility() != View::VISIBLE) { vg_parent->setVisibility(View::VISIBLE); }

    auto it = mMapConn.find(connId);
    if (it == mMapConn.end()) {
        LOGE("float not reg. id=%d", connId);
        return;
    }

    ConnItemData *dat = &it->second;
    if (mConnId != connId) {
        mConnId = connId;
        vg_parent->removeAllViews();

        if (!dat->vg) {
            if (std::find(mParents.begin(), mParents.end(), vg_parent) == mParents.end()) {
                mParents.push_back(vg_parent);
            }
            dat->vg = __dc(ViewGroup, getInflater()->inflate(dat->xml, vg_parent, true));
            if (dat->init) { dat->init(dat->vg); }
        } else {
            vg_parent->addView(dat->vg);
        }
    }

    if (dat->active) { dat->active(dat->vg); }
}

//////////////////////////////////////////////////////////////////////////////
// 公共方法
int regWindow(int windowID, IWindow *windPtr) {
    auto it = gMapWindows.find(windowID);
    if (it != gMapWindows.end()) {
        LOG(ERROR) << "window id exists. id=" << windowID << " class=" << windPtr->getClassName();
        return -1;
    }

    windPtr->setId(windowID);
    gMapWindows.insert(std::make_pair(windowID, windPtr));
    LOG(INFO) << "add window. id=" << windowID << " count=" << gMapWindows.size();

    return 0;
}

IWindow *getWindow(int windowID) {
    auto it = gMapWindows.find(windowID);
    if (it != gMapWindows.end()) { return it->second; }
    return nullptr;
}

void checkWindow() {
    // 检测窗口存在的时长，防止窗口未释放占用资源
}

int getWndCount() {
    return gMapWindows.size();
}

int getWindows(std::map<int, IWindow *> &wnds) {
    wnds = gMapWindows;
    return wnds.size();
}

void closeWindow(IWindow *windPtr) {
    if (!windPtr) return;

    auto it = gMapWindows.find(windPtr->getId());
    if (it != gMapWindows.end()) {
        gMapWindows.erase(it);
        LOG(INFO) << "release window. ID=" << windPtr->getId() << " Count=" << gMapWindows.size();
    } else {
        LOG(WARN) << "window not register, maybe allow repeat. ID=" << windPtr->getId();
    }

    windPtr->close();

    // 验证独立窗口时，没有主窗口
    if (gMapWindows.empty()) { LOG(ERROR) << "window list empty, please start from main window."; }
}

void getNumberPickerData(NumberDataType t, std::vector<int> &val, std::vector<std::string> &txt, int lanId /* = 0*/) {
    int b, e;

    b = 1;
    e = 10;

    val.clear();
    txt.clear();

    switch (t) {
    case NUMBER_PICKER_DT_YEAR: {
        static const int mid = 2023;
        b                    = mid - 10;
        e                    = mid + 10;
    } break;
    case NUMBER_PICKER_DT_MONTH: e = 12; break;
    case NUMBER_PICKER_DT_DAY: e = 31; break;
    case NUMBER_PICKER_DT_HOUR_12: {

        b = 0;
        e = 12;
        for (int i = b, j = 0; i <= e; i++, j++) {
            val.push_back(j);
            txt.push_back(fillLength(i, 2));
        }
        return;

    } break;
    case NUMBER_PICKER_DT_HOUR_24:
        b = 0;
        e = 23;
        break;
    case NUMBER_PICKER_DT_MINUTE: {
        b = 0;
        e = 59;
        for (int i = b, j = 0; i <= e; i++, j++) {
            val.push_back(j);
            txt.push_back(fillLength(i, 2));
        }
        return;
    } break;
    case NUMBER_PICKER_DT_DAY_PART: {
        static std::map<int, std::vector<std::string>> lan_day_part = {{0, {"上午", "下午", "上午", "下午"}},
                                                                       {1, {"AM", "PM", "AM", "PM"}}};
        txt                                                         = lan_day_part[lanId];
        for (int j = 0; j < txt.size(); j++) { val.push_back(j); }
        return;
    } break;
    case NUMBER_PICKER_DT_WEEK: {
        static std::map<int, std::vector<std::string>> lan_day_part = {
            {0, {"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"}},
            {1, {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}}};
        txt = lan_day_part[lanId];
        for (int j = 0; j < txt.size(); j++) { val.push_back(j); }
        return;
    } break;
    }

    for (int i = b, j = 0; i <= e; i++, j++) {
        val.push_back(j);
        txt.push_back(std::to_string(i));
    }
}

void setGroupEnabled(ViewGroup *vg, bool flag) {
    if (!vg) {
        LOG(DEBUG) << "viewgroup is null. flag=" << flag;
        return;
    }

    vg->setEnabled(flag);

    for (int i = 0; i < vg->getChildCount(); i++) {
        View      *sv  = vg->getChildAt(i);
        ViewGroup *svg = __dc(ViewGroup, sv);
        if (svg) {
            setGroupEnabled(svg, flag);
            continue;
        }

        CheckBox *chk = __dc(CheckBox, sv);
        if (chk) {
            chk->setChecked(!flag);
        } else {
            sv->setEnabled(flag);
        }
    }
}

void switchActivated(View *&oldv, View *newv) {
    if (!newv) return;
    if (!oldv) {
        oldv = newv;
        oldv->setActivated(true);
        return;
    }
    if (oldv->getId() != newv->getId()){
        oldv->setActivated(false);
        oldv = newv;
        oldv->setActivated(true);
    }
}

void removeAndDelAllViews(ViewGroup *vg) {
    if (!vg) return;
    std::vector<View *> view_child;
    for (int i = 0, j = vg->getChildCount(); i < j; i++) { view_child.push_back(vg->getChildAt(i)); }
    vg->removeAllViews();
    for (int i = 0, j = view_child.size(); i < j; i++) { delete view_child[i]; }
    view_child.clear();
}

bool hideView(View *v, int flag /* = View::GONE*/) {
    if (v->getVisibility() == View::VISIBLE) {
        v->setVisibility(flag);
        return true;
    }
    return false;
}

bool showView(View *v) {
    if (v->getVisibility() != View::VISIBLE) {
        v->setVisibility(View::VISIBLE);
        return true;
    }
    return true;
}

bool isShow(View *v) {
    return v->getVisibility() == View::VISIBLE;
}

void child2ParentRect(View *v, Rect &rc) {
    if (!v) return;
    View *parent, *my;
    my = v;
    while ((parent = my->getParent())) {
        rc.left += parent->getLeft();
        rc.top += parent->getTop();
        my = parent;
    }
}

void changeView(View *v, int hide_flag /* = View::GONE */) {
    if (v->getVisibility() != View::VISIBLE) {
        v->setVisibility(View::VISIBLE);
    } else {
        v->setVisibility(hide_flag);
    }
}

