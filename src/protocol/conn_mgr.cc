﻿
#include "conn_mgr.h"

#include <conf_mgr.h>
#include <data.h>
#include <wind_mgr.h>
#include <homewindow.h>

#define TICK_TIME 100 // tick触发时间（毫秒）

CConnMgr *g_objConnMgr = CConnMgr::ins();
//////////////////////////////////////////////////////////////////

CConnMgr::CConnMgr() {
    mPacket            = SDHWPacketBuffer::instance();
    mUartMCU           = 0;
    mNextEventTime     = 0;
    mLastSendTime      = 0;
    mLastStatusTime    = 0;
    mSetData           = 0;
    mLastButtonValue   = 0;
    mFurnaceDoorStatus = 0;
    mMcuPressVer       = 0;
    mMcuCtrlVer        = 0;
    mMcuType           = 0;
    mLastWarnTime      = 0;
    mLastBeepTime      = 0;

    g_objHandler->addHandler(0, this);
}

CConnMgr::~CConnMgr() {
    __del(mUartMCU);
}

int CConnMgr::init() {
    UartOpenReq ss;

    snprintf(ss.serialPort, sizeof(ss.serialPort), "/dev/ttyS1");
    ss.speed     = 9600;
    ss.flow_ctrl = 0;
    ss.databits  = 8;
    ss.stopbits  = 1;
    ss.parity    = 'N';

    mUartMCU = new UartClient(mPacket, BT_MCU, ss, "192.168.0.106", 9876, 0);
    mUartMCU->init();

    // 启动延迟一会后开始发包
    mNextEventTime = SystemClock::uptimeMillis() + TICK_TIME * 10;
    App::getInstance().addEventHandler(this);

    // 静音标识
    closeVolumn(g_objConf->getMuteFlag());

    return 0;
}

int CConnMgr::checkEvents() {
    int64_t curr_tick = SystemClock::uptimeMillis();
    if (curr_tick >= mNextEventTime) {
        mNextEventTime = curr_tick + TICK_TIME;
        return 1;
    }
    return 0;
}

int CConnMgr::handleEvents() {
    int64_t now_tick = SystemClock::uptimeMillis();

    if (mUartMCU) mUartMCU->onTick();

    if (mSetData > 0 || now_tick - mLastSendTime >= 1000) { send2MCU(); }

    return 1;
}

// 放水动作
void CConnMgr::setWaterAction(WaterAction t) {
    mSndData[BUF_SND_D3] = t;
    mSetData++;
}

// 水温设定
void CConnMgr::setWaterTem(int value) {
    mSndData[BUF_SND_D4] = value;
    mSetData++;
}

// 水量设定
void CConnMgr::setWaterYield(int value) {
    mSndData[BUF_SND_D5] = value >> 8;
    mSndData[BUF_SND_D6] = value & 0xff;
    mSetData++;
}

// 冰水和冰块 动作（是否制冷或制冰）
void CConnMgr::setCooling(CoolStatus setValue) {
    mSndData[BUF_SND_D7]  = setValue;
    mSetData++;
}

// 放冰块动作
void CConnMgr::setIceAction(bool flag) {
    mSndData[BUF_SND_D8] = flag? 1:0;
    mSetData++;
}

// PP 滤芯复位
void CConnMgr::setPPReset(bool flag){
    mSndData[BUF_SND_D9] = flag? 1:0;
    mSetData++;
}
// RO 滤芯复位
void CConnMgr::setROReset(bool flag){
    mSndData[BUF_SND_D10] = flag? 1:0;
    mSetData++;
}

void CConnMgr::setBeep(BeepType t, const char *from/* = 0*/) {
    LOGE("beepType=%d from=%s", t, (from ? from : ""));
    mSndData[BUF_SND_D12] = t;
    mSetData++;
}

void CConnMgr::closeVolumn(bool flag) {
   
    mSetData++;
}

void CConnMgr::setStoveLight(bool flag) {
    
    mSetData++;
}

void CConnMgr::setSleep(bool flag) {
   
    mSetData++;
}

void CConnMgr::send2MCU() {
    BuffData *bd = mPacket->obtain(BT_MCU);
    UI2MCU   snd(bd);
    for (int i = BUF_SND_D3; i < BUF_SND_D15; i++) { snd.setData(i, mSndData[i]); }

    //如果在UV杀菌的预约时间内
    if(g_appData.UVshajun == PRO_STATE_UV_RESERVATION){
        if(isTimeRange(g_appData.star_time_uv, g_appData.end_time_uv) != g_appData.isUVshajun){
            g_appData.isUVshajun = !g_appData.isUVshajun;
            Activity_header::ins()->changeState(PRO_STATE_UV_RESERVATION);
        }        
    }else if(g_appData.UVshajun == PRO_STATE_UV_KEEP_ENABLED){
        // 暂无操作 
    }
     

    // 放水动作（工作状态）
    snd.setData(BUF_SND_D3, g_appData.outWaterStatus);

    // 水温设定
    mSndData[BUF_SND_D4] = g_appData.outWaterTem;

    // 出冰块动作
    snd.setData(BUF_SND_D8, g_appData.iceStatus ? 1:0);

    // PP 滤芯复位
    snd.setData(BUF_SND_D9, g_appData.isResetPP ? 1:0);

    // RO 滤芯复位
    snd.setData(BUF_SND_D10, g_appData.isResetRO ? 1:0);

    // 如果是在预约UV杀菌的时间内
    if (g_appData.UVshajun == PRO_STATE_UV_KEEP_ENABLED) {
    }
    
    // 限制蜂鸣器响声间隔
    if (!g_appData.isMute && mSndData[BUF_SND_D12]) {
        int64_t now_tick = SystemClock::uptimeMillis();
        if (now_tick - mLastBeepTime < 500) {
            LOG(WARN) << "limit beep time. t=" << (int)mSndData[BUF_SND_D12] << " diff=" << (now_tick - mLastBeepTime);
            snd.setData(BUF_SND_D12, 0);
        } else {
            mLastBeepTime = now_tick;
        }
    }
    snd.checkcode();    // 修改检验位

    LOG(VERBOSE) << "send to mcu. bytes=" << hexstr(bd->buf, bd->len);
    mUartMCU->send(bd);
 
    mSetData      = 0;
    mLastSendTime = SystemClock::uptimeMillis();
}

bool CConnMgr::getVersion(int &a, int &b, int &c) {
    if (mMcuType == 0) return false;
    a = mMcuType;
    b = mMcuPressVer;
    c = mMcuCtrlVer;
    return true;
}

void CConnMgr::onCommDeal(IAck *ack) {
    LOG(VERBOSE) << "hex str=" << hexstr(ack->mBuf, ack->mDlen);
    int64_t now_tick = SystemClock::uptimeMillis();

    // 进水温度 （常温）
    if(g_appData.realWaterTem != ack->getData(BUF_RCV_D3)){
        HomeWindow::ins()->appStatusNotify(SC_INWATER_TEM,ack->getData(BUF_RCV_D3));
    }
    
    // 冰水温度
    if(g_appData.iceWaterTem != ack->getData(BUF_RCV_D4)){
        HomeWindow::ins()->appStatusNotify(SC_COOL_WATER_TEM, ack->getData(BUF_RCV_D4));
    }else if((ack->getData(BUF_RCV_D4) <= 12)&&(ack->getData(BUF_RCV_D4) >=6)&&(g_appData.zhileng == PRO_STATE_COOL_ING)){
        HomeWindow::ins()->appStatusNotify(SC_COOL_WATER_TEM,ack->getData(BUF_RCV_D4));
    }
    // 热水温度
    g_appData.hotWaterTem  = ack->getData(BUF_RCV_D5);

    // 净水
    if(g_appData.cleanWaterTDS != (ack->getData(BUF_RCV_D6)*255 + ack->getData(BUF_RCV_D7))){
        HomeWindow::ins()->appStatusNotify(SC_TDS_STATE, (ack->getData(BUF_RCV_D6)*255 + ack->getData(BUF_RCV_D7)));
    }
    // 纯水
    g_appData.prueWaterTDS    = (ack->getData(BUF_RCV_D8)*255 + ack->getData(BUF_RCV_D9));

    // 前置复合滤芯百分比（PP滤芯）
    uchar percenPP = ack->getData(BUF_RCV_D10);
    if (percenPP != 0) { HomeWindow::ins()->appStatusNotify(SC_PP_PERCEN,percenPP); }

    // RO滤芯百分比
    uchar percenRo = ack->getData(BUF_RCV_D11);
    if (percenRo != 0) { HomeWindow::ins()->appStatusNotify(SC_RO_PERCEN,percenRo); }
    // ( 0-机器正常运行 1-机器报警 )
    // 0bit:进水温探头报警  1bit:冰水温探头报警  2bit:热水温探头报警
    // 3bit:超温/干烧报警   4bit:制冷异常       5bit:制热异常  
    // 6bit:结冰报警        7bit 缺水报警  
    uchar warnCode = ack->getData(BUF_RCV_D12);
    if(warnCode != 0){
        LOGE("mcu warn  status1 ....  code=0x%02x", warnCode);
        if (now_tick - mLastWarnTime >= 30000) {        /* 30s提醒一次 */
            mLastWarnTime = now_tick;
            if(warnCode & 0x01) HomeWindow::ins()->appStatusNotify(SC_WARN_INTAKE_WATER_PROBE);
            if(warnCode & 0x02) HomeWindow::ins()->appStatusNotify(SC_WARN_ICE_WATER_PROBE);
            if(warnCode & 0x04) HomeWindow::ins()->appStatusNotify(SC_WARN_HOT_WATER_PROBE);
            if(warnCode & 0x08) HomeWindow::ins()->appStatusNotify(SC_WARN_OVER_DRY_HEATING);
            if(warnCode & 0x10) HomeWindow::ins()->appStatusNotify(SC_WARN_MAKE_COOL);
            if(warnCode & 0x20) HomeWindow::ins()->appStatusNotify(SC_WARN_MAKE_HOT);
            if(warnCode & 0x40) HomeWindow::ins()->appStatusNotify(SC_WARN_MAKE_ICE);
            if(warnCode & 0x80) HomeWindow::ins()->appStatusNotify(SC_WARN_STORTAGE_WATER);
        }
    } 

    // 0bit: 漏水报警       1bit：滤芯到期报警
    // 2bit：流量传感器报警  3bit：通讯故障（表示长时间未收到面板发往主板的信号）
    // 4bit：UV功能信号。1：表示正在杀菌，  0：表示杀菌停止
    // 5bit：冰满信号。  1：表示冰满，      0：表示未满
    // 6bit：冲洗信号。  1：表示在执行冲洗，0：表示冲洗停止。
    uchar status       = ack->getData(BUF_RCV_D13);
    if(status != 0){
        LOGE("mcu reset status2 .... code=0x%02x", status);
        if(status & 0x01) HomeWindow::ins()->appStatusNotify(SC_WARN_LEAKAGE_WATER);
        if(status & 0x02) HomeWindow::ins()->appStatusNotify(SC_WARN_LVXIN);
        if(status & 0x04) HomeWindow::ins()->appStatusNotify(SC_WARN_FLOW);
        if(status & 0x08) HomeWindow::ins()->appStatusNotify(SC_WARN_COMM_FAIL);
        if(status & 0x10) HomeWindow::ins()->appStatusNotify(SC_UV_COMPLETE);
        if(status & 0x20) HomeWindow::ins()->appStatusNotify(SC_ICE_COMPLETE);
        if(status & 0x40) HomeWindow::ins()->appStatusNotify(SC_WASH_COMPLETE);
        if(status & 0x80);
    }
    // （0:正常待机  1:以下功能）
    // (0bit):0 表示待机或放水中， 1 表示放水完成       
    // (1bit):0 待机或放冰块中，   1 表示出冰块完成
    // (2bit):0 待机或放冰块中，   1 表示 PP 滤芯复位完成  
    // (3bit):0 待机或放冰块中，   1 表示 RO 滤芯复位完成
    uchar waterLevel       = ack->getData(BUF_RCV_D14);
    if(waterLevel != 0){
        LOGE("mcu reset status3 .... code=0x%02x", waterLevel);
        if(waterLevel & 0x01) HomeWindow::ins()->appStatusNotify(SC_OUT_WATER_COMPLETE);
        if(waterLevel & 0x02) HomeWindow::ins()->appStatusNotify(SC_OUT_ICE_COMPLETE);
        if(waterLevel & 0x04) HomeWindow::ins()->appStatusNotify(SC_PP_RESET_COMPLETE);
        if(waterLevel & 0x08) HomeWindow::ins()->appStatusNotify(SC_RO_RESET_COMPLETE);
    }

    Activity_header::ins()->upHeader();
}
