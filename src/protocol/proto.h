﻿#ifndef __proto_h__
#define __proto_h__

// 交互协议相关定义
////////////////////////////////////////////////////////////////////////////////////////////////
// UI 发送给 MCU  （ 显示板 发送给 主板 ）

/* 放水动作
    0x00 表示不放水
    0x01 表示放热水
    0x02 表示放冰水
*/
typedef enum {
    WA_NULL         = 0,
    WA_HOT_WATER    = 1,
    WA_COLL_WATER   = 2,
} WaterAction;

/* 制冷、制冰状态
    0x00 面板上制冷按钮和制冰按钮都未按下 (此种状态压缩机不启动)
    0x01 面板上制冷按钮按下， 制冰按钮未按下
    0x02 面板上制冰按钮按下， 制冷按钮未按下
    0x03 面板上制冷和制冰块按钮同时按下
*/
typedef enum {
    CS_NULL     = 0, // 制冷制冰 均未启动
    CS_COOL     = 1, // 制冷
    CS_ICE      = 2, // 制冰(未用上，制冰需开启制冷，因此为 CS_COOL_ICE)
    CS_COOL_ICE = 3, // 制冷和制冰  
} CoolStatus;

// 辅助功能
typedef enum {
    HF_VOLUMN      = 1,      // 声音开关
    HF_STOVE_LIGHT = 1 << 1, // 炉灯
} HelperFunc;

// 蜂鸣器声音
typedef enum {
    BEEP_TYPE_POWERON         = 1, // 开机
    BEEP_TYPE_PRESSED         = 2, // 有效按键
    BEEP_TYPE_UNPRESS         = 3, // 无效按键
    BEEP_TYPE_PREHOT_COMPLETE = 4, // 预热完成
    BEEP_TYPE_AUTO_PAUSE      = 5, // 烹饪自动暂停
    BEEP_TYPE_RECIPES_OVER    = 6, // 烹饪结束
} BeepType;

////////////////////////////////////////////////////////////////////////////////////////////////
// MCU 发送给 UI  （ 主板 发送给 显示板 ）

typedef enum {
    SC_NULL = 0,    // 无状态更新

    SC_TDS_STATE,               // 自定义信号传输 TDS
    SC_INWATER_TEM,             // 进水温度、常温水温度变化
    SC_COOL_WATER_TEM,          // 制冷温度，冷水温度变化

    SC_WARN_CUSTOMER = 0x100,   // 自定义错误码
    SC_WARN_INTAKE_WATER_PROBE, // 进水温探头报警
    SC_WARN_ICE_WATER_PROBE,    // 冰水温探头报警
    SC_WARN_HOT_WATER_PROBE,    // 热水温探头报警
    SC_WARN_OVER_DRY_HEATING,   // 超温/干烧报警
    SC_WARN_MAKE_COOL,          // 制冷异常
    SC_WARN_MAKE_HOT,           // 制热异常
    SC_WARN_MAKE_ICE,           // 结冰报警
    SC_WARN_STORTAGE_WATER,     // 缺水报警

    SC_WARN_LEAKAGE_WATER,      // 漏水报警
    SC_WARN_LVXIN,              // 滤芯到期报警
    SC_WARN_FLOW,               // 流量传感器报警
    SC_WARN_COMM_FAIL,          // 通信故障(表示长时间未收到面板发往主板的信号)
    SC_UV_COMPLETE,             // UV功能完成
    SC_ICE_COMPLETE,            // 制冰完成(冰满)
    SC_WASH_COMPLETE,           // 冲洗完成
    

    SC_OUT_WATER_COMPLETE = 0x200,  // 放水完成
    SC_OUT_ICE_COMPLETE,        // 出冰块完成
    SC_PP_RESET_COMPLETE,       // PP滤芯复位完成
    SC_RO_RESET_COMPLETE,       // RO滤芯复位完成
    
    SC_PP_PERCEN,               // PP滤芯百分比
    SC_RO_PERCEN,               // RO滤芯百分比
}appStatusChange;

#endif
