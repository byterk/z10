﻿
#ifndef __conn_mgr_h__
#define __conn_mgr_h__

#include "packet_buffer.h"
#include "uart_client.h"
#include "cmd_handler.h"

class CConnMgr : public EventHandler, public IHandler {
protected:
    CConnMgr();
    ~CConnMgr();

public:
    static CConnMgr *ins() {
        static CConnMgr stIns;
        return &stIns;
    }

    // 放水动作
    void setWaterAction(WaterAction t);
    // 水温设定
    void setWaterTem(int value);
    // 水量设定
    void setWaterYield(int value);
    // 冰水和冰块 动作（是否制冷或制冰）
    void setCooling(CoolStatus setValue);
    // 放冰块动作
    void setIceAction(bool weight);
    // PP 滤芯复位
    void setPPReset(bool flag);
    // RO 滤芯复位
    void setROReset(bool flag);

    int init();
    // 发送给按键板
    void send2MCU();
    // 声音关闭
    void closeVolumn(bool flag);
    // 炉灯开关
    void setStoveLight(bool flag);
    // 待机状态[时钟]
    void setSleep(bool flag);
    // 蜂鸣器声音
    void setBeep(BeepType t, const char *from = 0);
    
    // 版本号
    bool getVersion(int &a, int &b, int &c);

protected:
    virtual int checkEvents();
    virtual int handleEvents();
    virtual void onCommDeal(IAck *ack);
private:
    int64_t          mLastStatusTime;
    IPacketBuffer   *mPacket;
    int64_t          mNextEventTime;
    int64_t          mLastSendTime;
    int64_t          mLastWarnTime;
    int64_t          mLastBeepTime;
    UartClient      *mUartMCU; // mcu
    int              mSetData; // 设置数据次数
    uchar            mSndData[UI2MCU::BUF_LEN]; // 发送缓冲区    
    uchar            mLastButtonValue; // 按键上一次键值
    uchar            mFurnaceDoorStatus; // 炉门上一次状态
    uchar            mMcuType;           // 机型
    uchar            mMcuPressVer;       // 按键板程序版本号
    uchar            mMcuCtrlVer;        // 电源板程序版本号
    
};

extern CConnMgr *g_objConnMgr;

#endif
