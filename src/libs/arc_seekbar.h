/************************************************************
 * Description: ���ν�����������ȱ��>20��
 * Author     : hookjc
 * C-Date     : 2023/4/29
 ************************************************************/

#ifndef __arc_seekbar_h__
#define __arc_seekbar_h__

#include "libs.h"

class ArcSeekBar : public View
{
public:    
    DECLARE_UIEVENT(void, OnArcSeekBarChangeListener, View &v, int progress);
    DECLARE_UIEVENT(void, OnArcSeekBarChangeListener2, View &v, int progress, bool fromUser);

public:
    ArcSeekBar(int w, int h);
    ArcSeekBar(Context *ctx, const AttributeSet &attr);
    ~ArcSeekBar();

    void setMin(int min);
    int  getMin();
    void setMax(int max);
    int  getMax();
    void setRange(int r0, int r2);
    void setProgress(int progress);
    int  getProgress();
    void setOnChangeListener(OnArcSeekBarChangeListener l);
    void setOnChangeListener2(OnArcSeekBarChangeListener2 l);
    void setShowSlider(bool Show);

protected:
    virtual void onMeasure(int widthMeasureSpec, int heightMeasureSpec);
    virtual void onDraw(Canvas &ctx);
    virtual bool onTouchEvent(MotionEvent &evt);

protected:
    static double ARC_EXTEND_ANGLE;
    enum emArcDrawStatus
    {
        ADS_DRAWING = 1,   // ������
        ADS_COMPLETE,      // �������
    };
    void  initViewData();
    float getRelativeDegrees(float x, float y);
    float getTouchDegrees(float x, float y);
    void  onDrawTimer();
    int   getWidthProgress();
    void  calcTouchState(int x, int y);
    void  checkRange(int &progress);

protected:
    int                        mWidth;             // 可绘制圆的宽度
    int                        mHeight;            // 可绘制圆的宽度
    int                        mCircleX;           // 圆弧中心x
    int                        mCircleY;           // 圆弧中心Y
    double                     mStartAngle;        // m/180.0*PI 起始角度
    double                     mEndAngle;          // M/180.0*PI 结束角度
    int                        mBackgroundColor;   // 背景色
    int                        mBackgroundWidth;   // 背景弧线宽度  
    int                        mForegroundColor;   // 前景色
    int                        mForegroundColor2;  // 前景色2
    Cairo::Context::LineCap    mStrokeCap;         // 线条端点样式
    int                        mForegroundWidth;   // 前景弧线宽度
    int                        mRadius;            // 圆弧半径
    int                        mMin;               // 最小值
    int                        mMax;               // 最大值
    int                        mProgress;          // 当前进度
    int                        mSliderRadius;      // 滑块半径
    int                        mSliderWidth;       // 滑块线条宽度
    int                        mSliderColor;       // 滑块线条颜色
    int                        mSliderFillColor;   // 滑块圆形填充颜色
    Drawable *                 mSlider;            // 滑块
    bool                       mShowSlider;        // 是否显示滑块
    RECT                       mRectSlider;        // 滑块区域
    bool                       mSliderDown;        // 是否点击滑块 
    int                        mDrawStatus;        // 滑块绘制状态
    bool                       mShowProgressTxt;   // 显示进度数值
    bool                       mIsPercentProgress; // 百分比进度
    Paint                      mPaintTxt;          // 文本属性
    OnArcSeekBarChangeListener mOnChangeListener;  // 滑块变化事件
    OnArcSeekBarChangeListener2 mOnChangeListener2;  // ����仯�¼�
    int                        mTouchLastX;
    int                        mTouchLastY;
    uchar                      mAngleAdd:1;        // ��������
    int                        mRange[2];          // �ɻ�����Χ
    bool                       mFromUser;

    int                        mSlectWidth;        // 可点击选中宽度  
};

#endif
