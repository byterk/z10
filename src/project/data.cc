#include "data.h"
#include "conf_mgr.h"

std::vector<WATER> modeData;

//读取数据
void readModeData(ViewGroup* container){
    std::vector<int>            array_type;
    std::vector<std::string>    array_item;
    std::vector<std::string>    array_picture;
    std::vector<std::string>    array_picture_2;
    std::vector<int>            array_wendu;
    std::vector<int>            array_chushuiliang;

    container->getContext()->getArray("@array/type",array_type);
    LOGD("array.size()=%d ",array_type.size());

    container->getContext()->getArray("@array/item",array_item);
    container->getContext()->getArray("@array/picture",array_picture);
    container->getContext()->getArray("@array/picture_2",array_picture_2);
    container->getContext()->getArray("@array/wendu",array_wendu);
    container->getContext()->getArray("@array/chushuiliang",array_chushuiliang);
    
    for(int i=0;i<array_type.size();i++){
        modeData.push_back({false,nullptr,array_type.at(i),array_item.at(i),array_picture.at(i),array_picture_2.at(i),array_wendu.at(i),array_chushuiliang.at(i)});
    } 
}



//////////////////////////////////////////////////////////////////////////////////////////////////
void setScreenLight(int light, bool chk /* = true*/) {
    // /sys/class/pwm/pwmchip0/pwmN/duty_cycle 数值就为pwm占空比，范围[1-99]
    char path_duty_cycle[] = "/sys/class/pwm/pwmchip0/pwm3/duty_cycle";
    if (access(path_duty_cycle, F_OK)) { return; }

    int pwm = (MAX_SCREEN_LIGHT - light) * 99 / MAX_SCREEN_LIGHT;
    if (chk) {
        if (pwm > 99) {
            pwm = (MAX_SCREEN_LIGHT - DEF_SCREEN_LIGHT) * 99 / MAX_SCREEN_LIGHT;   // 暗
        }
        if (pwm < 1) {
            pwm = 1;   // 亮
        }
    }

    char shell[128];
    snprintf(shell, sizeof(shell), "echo %d > %s", pwm, path_duty_cycle);
    system(shell);
}
