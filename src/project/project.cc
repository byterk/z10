#include "project.h"

AppGlobalData  g_appData;

void WindowTime::delView(TextView *timeView){
    if (!timeView)
        return;
    for (auto it = mTimeView.begin(); it != mTimeView.end(); it++)
    {
        if (*it == timeView)
        {
            mTimeView.erase(it);
            return;
        }
    }
    LOG(ERROR) << "not find the timeView. id=" << timeView->getId();
}

void WindowTime::addView(TextView *timeView)
{
    timeView->setText(getTimeStr());
    mTimeView.push_back(timeView);
}

void WindowTime::updTime()
{   
    std::string showTime = getTimeStr();

    for (int i = 0, j = mTimeView.size(); i < j; i++)
    {
        mTimeView[i]->post([this,i,showTime](){
            mTimeView[i]->setText(showTime);
        });
        
    }
}

// 判断 现在是不是时间范围内
bool isTimeRange(std::tm star_time, std::tm end_time){
    std::time_t now_tick = std::time(NULL);
    std::tm tm_now = *localtime(&now_tick);

    std::tm tm_star = tm_now;
    std::tm tm_end = tm_now;
    tm_star.tm_hour = star_time.tm_hour; 
    tm_star.tm_min = star_time.tm_min;
    tm_end.tm_hour = end_time.tm_hour; 
    tm_end.tm_min = end_time.tm_min;

    int64_t star_tick = std::mktime(&tm_star);
    int64_t end_tick = std::mktime(&tm_end);
    // 开始的时钟大于结束的时钟，比如： 20：00 - 02：00 此时结束时间应该为第二天，需要添加一天
    if(star_time.tm_hour > end_time.tm_hour){
        end_tick += 86400;
    }
    
    if((star_tick < now_tick) && (now_tick < end_tick))
        return true;
    
    return false;
}
