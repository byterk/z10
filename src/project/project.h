#ifndef __PROJECT_H__
#define __PROJECT_H__

#include <comm_func.h>
#include <common.h>
#include <proto.h>

#include "R.h"

// 业务窗口ID
enum emWindowID {
    WINDOW_NONE = 100000,
};

#define LVXIN_WARN 100
#define LVXIN_SON  75

// 程序状态
typedef enum ProgramState {
    PRO_STATE_NONE = 0,                     // 某个功能无状态

    PRO_STATE_TONGSUO,                      // 童锁开启
    PRO_STATE_TONGSUO_CLOSE,                // 童锁关闭

    PRO_STATE_LVXIN_NORMAL,                 // 滤芯正常
    PRO_STATE_LVXIN_SOON,                   // 滤芯准备过期
    PRO_STATE_LVXIN_EXPIRED,                // 滤芯已经过期

    PRO_STATE_UV_NONE,                      // UV杀菌 未开启
    PRO_STATE_UV_KEEP_ENABLED,              // UV杀菌 保持开启
    PRO_STATE_UV_RESERVATION,               // UV杀菌 预约时间开启
    PRO_STATE_UV_USER,                      // UV杀菌 手动进行 杀菌

    PRO_STATE_TDS_SHOW,                     // TDS 显示
    PRO_STATE_TDS_HIDE,                     // TDS 隐藏
    PRO_STATE_ZHAOMING,                     // 照明开启
    PRO_STATE_ZHAOMING_CLOSE,               // 照明关闭

    PRO_STATE_COOL_NONE,                    // 没有开启制冷
    PRO_STATE_COOL_ING,                     // 正在制冷
    PRO_STATE_COOL_DONE,                    // 已完成制冷

    PRO_STATE_ICE_NONE,                     // 没有开启制冰
    PRO_STATE_ICE_ING,                      // 正在制冰
    PRO_STATE_ICE_DONE,                     // 已完成制冰

    PRO_STATE_NET_NONE,                     // 网络 没信号
    PRO_STATE_NET_ONE,                      // 网络 一格信号
    PRO_STATE_NET_TWO,                      // 网络 两格信号
    PRO_STATE_NET_THREE,                    // 网络 三格信号
    PRO_STATE_NET_FULL,                     // 网络 满信号

    PRO_STATE_SETING,                       // 设置

    PRO_STATE_WASH,                         //开启了自定义 清洗 
    
}State;

// 应用全局数据
typedef struct tagAppGlobalData {
    bool isChange;      //检查是否更新了状态

    int   netStatus;     // 网络状态
    bool  isMute;        // 静音

    WaterAction  outWaterStatus;   // 出水状态
    int          outWaterTem;   // 出水温度
    CoolStatus   coolStatus;    // 制冷、制冰状态(暂未替换)
    bool  iceStatus;     // 出冰状态
    bool  isResetPP;     // 重置PP滤芯
    bool  isResetRO;     // 重置RO滤芯
    int   realWaterTem;  // 实时温度
    int   iceWaterTem;   // 冰水温度
    int   hotWaterTem;   // 热水温度
    int   totalWater;    // 总过滤水量
    int   cleanWaterTDS;    // 净水TDS
    int   prueWaterTDS;     // 纯水TDS
    bool  isWashRO;      // 是否正在执行冲洗RO滤芯

    uchar warnState;     // 机器报警

    int   childLocked;   // 童锁
    int   lvxin;         // 滤芯
    int   PPLvxinPercen; // PP滤芯百分比
    int   ROLvxinPercen; // RO滤芯百分比
    int   UVshajun;      // UV杀菌
    bool  isUVshajun;    // 是否正在UV杀菌
    int   showStatusTDS; // 是否显示TDS 
    int   zhaoming;      // 照明
    int   zhileng;       // 制冷
    int   zhibing;       // 制冰
    
    int   light;         // 亮度

    std::tm star_time_uv;   //uv杀菌的预约 开始时间
    std::tm end_time_uv;    //uv杀菌的预约 结束时间

    std::tm star_time_wash; //自定义清洗 开始时间
    std::tm end_time_wash;  //自定义清洗 结束时间
    
    tagAppGlobalData()
        : isChange(true),netStatus(PRO_STATE_NET_NONE),isMute(false),outWaterStatus(WA_NULL),outWaterTem(25),childLocked(PRO_STATE_TONGSUO_CLOSE),
            iceStatus(false),isResetPP(false),isResetRO(false),realWaterTem(25),iceWaterTem(8),hotWaterTem(80),totalWater(0),
            lvxin(PRO_STATE_LVXIN_NORMAL),UVshajun(PRO_STATE_UV_KEEP_ENABLED),isUVshajun(false),showStatusTDS(PRO_STATE_TDS_HIDE),zhaoming(PRO_STATE_ZHAOMING),
            zhileng(PRO_STATE_COOL_NONE),zhibing(PRO_STATE_ICE_NONE),light(70){}
} AppGlobalData, APPGLOBALDATA, *LPAPPGLOBALDATA;

extern AppGlobalData g_appData;

///////////////////////////////////////////////////////////////////////////////////////////////
// 所有页面的时间更新
class WindowTime {
public:
    static WindowTime *ins() {
        static WindowTime ins;
        return &ins;
    }

    void delView(TextView *timeView);
    void addView(TextView *timeView);
    void updTime();

protected:
    WindowTime() {}
    ~WindowTime() {}

private:
    std::vector<TextView *> mTimeView;
    int mtime_state;
};

bool isTimeRange(std::tm star_time, std::tm end_time);

#endif