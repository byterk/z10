﻿
#ifndef __conf_mgr_h__
#define __conf_mgr_h__

#include <unistd.h>
#include <cdroid.h>
#include <core/preferences.h>

#include "project.h"

class CConfMgr
{
public:
    static CConfMgr *ins()
    {
        static CConfMgr stIns;
        return &stIns;
    }

    int  init();
    void update();

    // 首次初始化
    bool isInitSet();
    void setInitSetFlag(bool flag);

    // 童锁
    bool getChildLockFlag();
    void setChildLockFlag(bool flag);

    // 静音
    bool getMuteFlag();
    void setMuteFlag(bool flag);

    // 音量
    int  getVolume();
    void setVolume(int value);

    // 亮度
    int  getLight();
    void setLight(int value);
    // 请求升级版本号地址
    std::string getCheckUpdateURL();

    // 恢复出厂设置
    void reset();

protected:
    CConfMgr();
    void createConfig();

private:
    Preferences mPrefer;   // 配置文件读写
    std::string mFileName; // 配置文件名
};

extern std::vector<CLA::Argument> g_stCustomCLA;
extern CConfMgr *g_objConf;

#endif
