﻿
#include "conf_mgr.h"
#include "data.h"

#define SECTION_INIT     "init"

std::vector<CLA::Argument> g_stCustomCLA = {
    {CLA::EntryType::Option, "p", "page", "测试窗口展示测试页", CLA::ValueType::Int, (int)CLA::EntryFlags::Manditory},
    {CLA::EntryType::Option, "w", "wind", "测试窗口类", CLA::ValueType::Int, (int)CLA::EntryFlags::Manditory},
    {CLA::EntryType::Option, "m", "model", "机型(蒸|微|纯)烤-[62000|62001|62002]", CLA::ValueType::Int, (int)CLA::EntryFlags::Manditory}
};

CConfMgr *g_objConf = nullptr;

CConfMgr::CConfMgr(){
    g_objConf = this;
}

int CConfMgr::init()
{
    std::string bak_file_name;
    mFileName = App::getInstance().getName() + ".xml";
    bak_file_name = mFileName + ".bak";

    LOG(DEBUG) << "load config. file=" << mFileName;

    if (access(mFileName.c_str(), F_OK) == 0) {
        mPrefer.load(mFileName);
    } else if (access(bak_file_name.c_str(), F_OK) == 0) {
        mPrefer.load(bak_file_name);
    }
    if ((getVolume() < 0 || getLight() < 0) && access(bak_file_name.c_str(), F_OK) == 0){
        // 声音和亮度为0，配置文件可能损坏
        mPrefer.load(bak_file_name);
    }

    // 配置文件损坏，重新创建
    if (getVolume() < 0 || getLight() < 0) {
        createConfig();
        mPrefer.load(mFileName);
    }

    // loadGlobalData();

    g_appData.isMute = getMuteFlag();

    // 系统初始日期时间
    time_t tt_now    = time(0);
    struct tm tm_now = *localtime(&tt_now);
    if (tm_now.tm_year < 2023) {
        timeSet(2023, 1, 1, 0, 0, 0);
    }

    return 0;
}

void CConfMgr::createConfig(){
    FILE *pf = fopen(mFileName.c_str(), "wb");
    if (!pf) return;

    char buf[] = {
        "<sections>\n"
        "  <section name=\"init\" desc=\"初始信息\">\n"
        "    <item name=\"init_set\" desc=\"是否初始化设置\">false</item>\n"
        "    <item name=\"child_lock\" desc=\"童锁\">false</item>\n"
        "    <item name=\"mute\" desc=\"静音\">false</item>\n"
        "    <item name=\"volume\" desc=\"音量\">50</item>\n"
        "    <item name=\"light\" desc=\"亮度\">50</item>\n"
        "    <item name=\"check_update\">http://119.23.42.165/html/xphp/index.php?api=sdhw_update</item>\n"
        "  </section>\n"
        "</sections>\n"
    };

    fwrite(buf, strlen(buf), 1, pf);

    fflush(pf);
    fclose(pf);
}

void CConfMgr::update()
{
    if (mPrefer.getUpdates() > 0)
    {
        mPrefer.save(mFileName);
        mPrefer.save(mFileName + ".bak");
        LOG(DEBUG) << "save config. file=" << mFileName;
    }
}

// 连接wifi提示
bool CConfMgr::isInitSet()
{
    return mPrefer.getBool(SECTION_INIT, "init_set", false);
}

void CConfMgr::setInitSetFlag(bool flag)
{
    mPrefer.setValue(SECTION_INIT, "init_set", flag);
}

// 童锁
bool CConfMgr::getChildLockFlag()
{
    return mPrefer.getBool(SECTION_INIT, "child_lock");
}

void CConfMgr::setChildLockFlag(bool flag)
{
    mPrefer.setValue(SECTION_INIT, "child_lock", flag);
}


// 静音
bool CConfMgr::getMuteFlag()
{
    return mPrefer.getBool(SECTION_INIT, "mute");
}

void CConfMgr::setMuteFlag(bool flag)
{
    mPrefer.setValue(SECTION_INIT, "mute", flag);
}

// 音量
int CConfMgr::getVolume()
{
    return mPrefer.getInt(SECTION_INIT, "volume", -1);
}

void CConfMgr::setVolume(int value)
{
    mPrefer.setValue(SECTION_INIT, "volume", value);
}

// 亮度
int CConfMgr::getLight()
{
    return mPrefer.getInt(SECTION_INIT, "light", 50);
}

void CConfMgr::setLight(int value)
{
    if (value < 0) value = 0;
    mPrefer.setValue(SECTION_INIT, "light", value);
}

std::string CConfMgr::getCheckUpdateURL(){
    return mPrefer.getString(SECTION_INIT, "check_update");
}

void CConfMgr::reset(){
    // 设置亮度 50
    setLight(50);
    setScreenLight(50);

    // 设置声音
    setVolume(50);
}
