#ifndef __DATA_H__
#define __DATA_H__

#include <iostream>
#include <stdio.h>
#include <vector>

#include "project.h"

#define MAX_SCREEN_LIGHT 100 // 屏幕最大亮度值
#define DEF_SCREEN_LIGHT 50 // 屏幕默认亮度

typedef enum{
    MODE_TYPE_ICE = 1000,   // 冰块
    MODE_TYPE_COOL_WATER,   // 冷水
    MODE_TYPE_SIMPLE_WATER, // 常温水
    MODE_TYPE_HOT_WATER,    // 沸水
    MODE_TYPE_UER,          // 非特殊模式
}Mode_Type;

// 出水、出冰 的数据结构体
typedef struct{
    bool isChange;
    LinearLayout *myins;

    int type;
    std::string item;
    std::string picture;
    std::string picture_2;
    int wendu;
    int chushuiliang;
}WATER;

//出水、出冰 的全部数据
extern std::vector<WATER> modeData;

//读取数据（出水、出冰）
void readModeData(ViewGroup* container);

// 设置屏幕亮度
void setScreenLight(int light, bool chk = true);
#endif