﻿
#include "spi.h"
#include <cdlog.h>
#include <string.h>

#define pabort(S) \
    printf(S);    \
    exit(0)

static int fd         = 0;
static int g_bits     = 8;
static int g_speed    = 64000;
static int g_delay_us = 10000;

int spi_init(char *spi_dev, int mode, int nits, int speed)
{
    fd = open(spi_dev, O_RDWR);
    if (fd < 0)
    {
        printf("[%s]:[%d] open spi file error\n", __FUNCTION__, __LINE__);
        return -1;
    }
    //设置模式
    int ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    if (ret == -1)
        pabort("can't set spi mode");

    ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
    if (ret == -1)
        pabort("can't get spi mode");

    //设置一次多少位
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &nits);
    if (ret == -1)
        pabort("can't set bits per word");

    ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &nits);
    if (ret == -1)
        pabort("can't get bits per word");

    //设置最大速度
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        pabort("can't set max speed hz");

    ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        pabort("can't get max speed hz");

    printf("spi mode: %d\n", mode);
    printf("bits per word: %d\n", nits);
    printf("max speed: %d Hz (%d KHz)\n", speed, speed / 1000);

    g_bits  = nits;
    g_speed = speed;

    return 0;
}

int spi_readwrte(unsigned char *txbuf, int txlen, unsigned char *rxbuf, int rxlen)
{
    struct spi_ioc_transfer  xfer[2];
    unsigned char           *bp;
    int                      i, status;

	memset(xfer, 0, sizeof xfer);

    xfer[0].tx_buf = (__u64)txbuf;
    xfer[0].len    = txlen;

    xfer[1].rx_buf = (__u64)rxbuf;
    xfer[1].len    = rxlen;

	printf("request(%2d, %2d): ", txlen, 0);
    for (bp = txbuf, i = 0; i < txlen; i++)
	{
        printf("%02x ", *bp++);
    }
    printf("\n");

	status         = ioctl(fd, SPI_IOC_MESSAGE(2), xfer);
    if (status < 0)
    {
        perror("SPI_IOC_MESSAGE");
        return -1;
    }

    printf("response(%2d, %2d): ", rxlen, status);
    for (bp = rxbuf, i = 0; i < rxlen; i++)
	{
        printf("%02x ", *bp++);
    }
    printf("\n");

    return 0;
}

int spi_close()
{
    close(fd);
    return 0;
}
