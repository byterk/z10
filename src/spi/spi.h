﻿
#ifndef __spi_h__
#define __spi_h__

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

int spi_init(char *spi_dev,int mode,int nits,int speed);

int spi_readwrte(unsigned char *txbuf, int txlen, unsigned char *rxbuf, int rxlen);

int spi_close();

#endif

/*/spi
    char dev_name[] = "/dev/spi1.0";

    unsigned char snd_buf[20] = {0};
    unsigned char rcv_buf[20] = {0};

    snd_buf[0] = 0x5a;
    snd_buf[1] = 0x90;
    snd_buf[2] = 0x42;
    for (int i = 3; i <= 12; i++)
    {
        snd_buf[i] = 1;
    }
    ushort crc_val = CRC16_CCITT(snd_buf, 13);
    snd_buf[18] = crc_val >> 8;
    snd_buf[19] = crc_val & 0xff;

	spi_init(dev_name, SPI_CPHA | SPI_CPOL, 8, 64000);
	spi_readwrte(snd_buf, 20, rcv_buf, 20);

	spi_close();
*/