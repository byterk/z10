﻿
#include <comm_func.h>
#include "socket_client.h"

#define RPORT_LEN 0 // 端口往后顺序找的范围
#define CONN_TIMEOUT 2
#define CONN_SPACE 2

SocketClient::SocketClient() {
    mIp           = "127.0.0.1";
    mPort         = 19090;
    mRPort        = mPort + RPORT_LEN;
    mLastConnTime = 0;
    mSockId       = -1;
}

SocketClient::~SocketClient() { }

int SocketClient::init(const char *ip, ushort port) {
    if (ip) mIp = ip;
    if (port > 0) mPort = port;
    mRPort = mPort + RPORT_LEN;
    return 0;
}

void SocketClient::onTick() {
    int64_t cur_time = SystemClock::currentTimeSeconds();

    if (isConn()) {
        if (isTimeout()) {
            LOG(ERROR) << "Conn time out, close it. snd=" << mLastSndTime << " rcv=" << mLastRcvTime;
            mStatus = ST_DISCONNECT;
            onStatusChange();
        }
    } else {
        if (mStatus == ST_CONNECTING) {
            onCheckConnecting();
        } else if (cur_time - mLastConnTime >= CONN_SPACE) {
            mLastConnTime = cur_time;
            connectServer();
        }
    }
}

bool SocketClient::connectServer() {
    int                sockfd;
    struct sockaddr_in server_addr;

    if (mStatus != ST_DISCONNECT) {
        return true;
    }

    // 创建Socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        LOGE("Error in creating socket, error=%d", errno);
        return false;
    }

    // 将Socket设置为非阻塞模式
    int flags = fcntl(sockfd, F_GETFL, 0);
    fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);

    // 设置服务器地址和端口
    server_addr.sin_family      = AF_INET;
    server_addr.sin_port        = htons(mPort);
    server_addr.sin_addr.s_addr = inet_addr(mIp.c_str());

    LOG(DEBUG) << "ip=" << mIp << " port=" << mPort << " ptr=" << this;

    // 连接到服务器
    mLastConnTime      = SystemClock::currentTimeSeconds();
    int connect_status = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (connect_status < 0) {
        if (errno != EINPROGRESS) {
            LOGE("Error in connecting to server. port=%d err=%d errmsg=%s", mPort, errno, strerror(errno));
            close(sockfd);
            if (++mPort > mRPort) {
                mPort = mRPort - RPORT_LEN;
            }
            return false;
        }
        LOGD("connecting... fd=%d", sockfd);
        mSockId = sockfd;
        mStatus = ST_CONNECTING;
        return true;
    }

    LOG(DEBUG) << "client connect ok. ip=" << mIp << " port=" << mPort << " fd=" << sockfd;
    setFd(sockfd);

    return true;
}

bool SocketClient::onCheckConnecting() {
    if (mStatus != ST_CONNECTING || mSockId <= 0) {
        LOGE("do not check connecting... fd=%d", mSockId);
        return false;
    }

    int64_t now_seconds = SystemClock::currentTimeSeconds();
    if (now_seconds - mLastConnTime > CONN_TIMEOUT) {
        LOGE("Error in connecting to server with timeout. port=%d", mPort);
        mStatus = ST_DISCONNECT;
        close(mSockId);
        mSockId = -1;
        if (++mPort > mRPort) {
            mPort = mRPort - RPORT_LEN;
        }
        return true;
    }

    fd_set writeset;
    FD_ZERO(&writeset);
    FD_SET(mSockId, &writeset);

    //可以利用tv_sec和tv_usec做更小精度的超时控制
    struct timeval tv;
    tv.tv_sec  = 0;
    tv.tv_usec = 10000;

    if (select(mSockId + 1, NULL, &writeset, NULL, &tv) == 1) {
        LOG(DEBUG) << "client connect ok. ip=" << mIp << " port=" << mPort << " fd=" << mSockId;
        setFd(mSockId);
    }

    return true;
}

int SocketClient::onRecvData() {
    LOG(DEBUG) << "data len is " << mRSBuf->last - mRSBuf->pos;
    return 0;
}

void SocketClient::onStatusChange() {
    if (mStatus == ST_DISCONNECT) {
        mSockId = -1;
    }
    Client::onStatusChange();
}
