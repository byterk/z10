﻿
#ifndef __app_update_h__
#define __app_update_h__

#include "curl_manager.h"

#define UPDATE_FILE    "update.txt"     // 更新检查文件，文件内容为[app_versioin=app_Build_x.x.x.x_gitversion_date]\nappkey=
#define UPDATE_APP_VER "app_ver="
#define UPDATE_APP_KEY "app_key="
#define UPDATE_NO "0"

/////////////////////////////////////////////////////////////////////////////////////
class AppUpdate : public CURLCallback {
public:
    DECLARE_UIEVENT(void, OnCheckListener, int result, const std::string &new_ver);

public:
    AppUpdate();
    ~AppUpdate();

    int init(const std::string &check_url);

    void setAppVer(const std::string &app_ver);
    void setAppKey(const std::string &app_key);

    bool checkUpdate();
    bool startUpdate();

    void setOnCheckListener(OnCheckListener l);

protected:
    virtual void onHttpResponse(int result, const std::string &body, void *data);

private:
    std::string     mCheckUrl;        // 检查地址
    std::string     mAppVer;          // 版本信息
    std::string     mAppKey;          // appkey
    OnCheckListener mOnCheckListener; // 接收到版本数据回调
};

#endif
