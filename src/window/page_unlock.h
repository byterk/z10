#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>

#include <homewindow.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

class Page_Unlock:public Window{
private:
    Page_Unlock();
    ~Page_Unlock();
    
    Runnable mRunnable;

    void onSeekBarChange(cdroid::AbsSeekBar& seekbar, int progress, bool fromUser);
public:
    static Page_Unlock *mins;
    static Page_Unlock *ins() {
        if(mins == nullptr)
            mins = new Page_Unlock;
        return mins;
    }

    static bool isPresent();
    
};