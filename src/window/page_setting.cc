#include <page_setting.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

Page_setting::Page_setting() :Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/page_setting", this);

    auto box_click_func = std::bind(&Page_setting::onSettingBoxClick, this, std::placeholders::_1);
    auto switch_click_func = std::bind(&Page_setting::onSettingSwClick, this, std::placeholders::_1);

    findViewById(z10::R::id::setting_screenLight)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_washTime)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_modechoose)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_net)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_time)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_uv)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_lvxin)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_version)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::setting_reset)->setOnClickListener(box_click_func);
    findViewById(z10::R::id::title_back)->setOnClickListener(box_click_func);

    mSwCool = (Switch*)findViewById(z10::R::id::setting_bt_cryogen);
    mSwIce = (Switch*)findViewById(z10::R::id::setting_bt_ice);
    mSwLock = (Switch*)findViewById(z10::R::id::setting_bt_lock);

    mSwCool->setOnClickListener(switch_click_func);
    mSwIce->setOnClickListener(switch_click_func);
    mSwLock->setOnClickListener(switch_click_func);

    mSwCool->setChecked(g_appData.zhileng == PRO_STATE_COOL_NONE ? false : true);
    mSwIce->setChecked(g_appData.zhibing == PRO_STATE_ICE_NONE ? false : true);
    mSwLock->setChecked(g_appData.childLocked == PRO_STATE_TONGSUO_CLOSE ? false : true);
}

Page_setting::~Page_setting() {
}

void Page_setting::onSettingBoxClick(View& v) {
    LOGD("click %d", v.getId());
    switch (v.getId()) {
        case z10::R::id::setting_screenLight: {
            ScreenLightWindow* w = new ScreenLightWindow;
            break;
        }

        case z10::R::id::setting_washTime: {
            Page_set_wash* w = new Page_set_wash;
            break;
        }

        case z10::R::id::setting_modechoose: {
            ModeWindow* w = new ModeWindow(this);
            break;
        }

        case z10::R::id::setting_net: {
            NetWorkWindow* w = new NetWorkWindow;
            break;
        }

        case z10::R::id::setting_time: {
            Page_set_time* w = new Page_set_time;
            break;
        }

        case z10::R::id::setting_uv: {
            Page_uv* w = new Page_uv();
            break;
        }

        case z10::R::id::setting_lvxin: {
            LvXinWindow* w = new LvXinWindow;
            break;
        }

        case z10::R::id::setting_version: {
            break;
        }

        case z10::R::id::setting_reset: {
            break;
        }

        case z10::R::id::title_back: {
            Activity_header::ins()->upHeader();
            this->close();
            break;
        }

        default:
            break;
    }
}

void Page_setting::onSettingSwClick(View& v) {
    switch (v.getId()) {
        case z10::R::id::setting_bt_cryogen:{
            if (!mSwCool->isChecked() && !!mSwIce->isChecked()) {
                LOGD("关闭制冷同时关闭制冰");
                mSwIce->setChecked(false);
                g_appData.zhibing = PRO_STATE_ICE_NONE;
            }

            g_appData.zhileng = mSwCool->isChecked() ? PRO_STATE_COOL_ING : PRO_STATE_COOL_NONE;
            g_appData.isChange = true;
            LOGD(mSwCool->isChecked() ? "开启制冷" : "关闭制冷");
            break;
        };
        case z10::R::id::setting_bt_ice:{
            if (!!mSwIce->isChecked() && !mSwCool->isChecked()) {
                LOGD("关闭制冷同时关闭制冰");
                mSwCool->setChecked(true);
                g_appData.zhileng = PRO_STATE_COOL_ING;
            }

            g_appData.zhibing = mSwIce->isChecked() ? PRO_STATE_ICE_ING : PRO_STATE_ICE_NONE;
            g_appData.isChange = true;
            LOGD(mSwCool->isChecked() ? "开启制冰" : "关闭制冰");
            break;
        };
        case z10::R::id::setting_bt_lock:{
            g_appData.childLocked = mSwLock->isChecked() ? PRO_STATE_TONGSUO : PRO_STATE_TONGSUO_CLOSE;
            g_appData.isChange = true;
            LOGD(mSwLock->isChecked() ? "开启童锁" : "关闭童锁");
            break;
        };

        default:
            break;
    }
}