#include <page_set_wash.h>

Page_set_wash::Page_set_wash() :Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/page_set_wash", this);

    mPickeStarHour = (NumberPicker*)findViewById(z10::R::id::numberpicker_star_hour);
    mPickeStarMin = (NumberPicker*)findViewById(z10::R::id::numberpicker_star_min);
    mPickeEndHour = (NumberPicker*)findViewById(z10::R::id::numberpicker_end_hour);
    mPickeEndMin = (NumberPicker*)findViewById(z10::R::id::numberpicker_end_min);

    mPickeStarHour->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeStarMin->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeEndHour->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeEndMin->setFormatter([this](int value){ return fillLength(value, 2); });

    findViewById(z10::R::id::linearLayout)->setOnClickListener(std::bind(&Page_set_wash::onClickBackListener, this, std::placeholders::_1));
    findViewById(z10::R::id::setting_screenLight_Box)->setOnClickListener(std::bind(&Page_set_wash::onClickBackListener, this, std::placeholders::_1));
    findViewById(z10::R::id::set_wash_cancle)->setOnClickListener(std::bind(&Page_set_wash::onClickBackListener, this, std::placeholders::_1));
    findViewById(z10::R::id::set_wash_enter)->setOnClickListener(std::bind(&Page_set_wash::onClickBackListener, this, std::placeholders::_1));
}

Page_set_wash::~Page_set_wash() {
    delete mPickeStarHour;
    delete mPickeStarMin;
    delete mPickeEndHour;
    delete mPickeEndMin;
}
void Page_set_wash::onClickBackListener(View& v) {
    switch (v.getId()) {
        case z10::R::id::setting_screenLight_Box:
            return;
        case z10::R::id::linearLayout:
        case z10::R::id::set_wash_cancle:
            break;
        case z10::R::id::set_wash_enter:
            g_appData.star_time_wash.tm_hour = mPickeStarHour->getValue();
            g_appData.star_time_wash.tm_min = mPickeStarMin->getValue();
            g_appData.end_time_wash.tm_hour = mPickeEndHour->getValue();
            g_appData.end_time_wash.tm_min = mPickeEndMin->getValue();

            LOGD("自定义清洗时间为   %d:%d - %d:%d", g_appData.star_time_wash.tm_hour, g_appData.star_time_wash.tm_min, g_appData.end_time_wash.tm_hour, g_appData.end_time_wash.tm_min);
            break;
    }
    this->close();
}