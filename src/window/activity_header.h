#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>

#include <homewindow.h>
#include <data.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

class Activity_header{
private:
    Runnable mRunnable;
    AppGlobalData mg_appData;

    ImageView *mTongsuo;
    ImageView *mLvxin;
    TextView  *mUVshajun;
    TextView  *mTDS;
    ImageView *mZhaoming;
    TextView  *mZhileng;
    TextView  *mZhibing;
    TextView  *mTime;
    ImageView *mNet;
    ImageView *mSetup;

    void onButtonClick(View&v);
public:
    static ViewGroup* parent;
    static Activity_header *ins() {
        static Activity_header ins;
        return &ins;
    }
    static void set_Parentview(ViewGroup* container){
        parent = container;
    }
    void init();
    void upHeader();          // 更新全部状态
    State checkState();       // 检查状态
    void changeState(State state);      // 更新某个状态

    Activity_header();
    ~Activity_header();
};