#include <page_set_time.h>

Page_set_time::Page_set_time():Window(0,0,-1,-1){

    LayoutInflater::from(getContext())->inflate("@layout/page_setting_time",this);
    mPickerYear = (NumberPicker *)findViewById(z10::R::id::numberpicker_set_year);
    mPickerMon = (NumberPicker *)findViewById(z10::R::id::numberpicker_set_mon);
    mPickerDay = (NumberPicker *)findViewById(z10::R::id::numberpicker_set_day);
    mPickeHour = (NumberPicker *)findViewById(z10::R::id::numberpicker_set_hour);
    mPickeMin = (NumberPicker *)findViewById(z10::R::id::numberpicker_set_min);

    std::time_t now_tick = std::time(NULL);
    std::tm tm_now = *localtime(&now_tick);

    mPickerYear->setValue(tm_now.tm_year + 1900);
    mPickerMon->setValue(tm_now.tm_mon + 1);
    mPickerDay->setValue(tm_now.tm_mday);
    mPickeHour->setValue(tm_now.tm_hour);
    mPickeMin->setValue(tm_now.tm_min);

    mPickerMon->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickerDay->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeHour->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeMin->setFormatter([this](int value){ return fillLength(value, 2); });

    mPickerYear->setOnValueChangedListener(std::bind(&Page_set_time::onValueChangedListener,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
    mPickerMon->setOnValueChangedListener(std::bind(&Page_set_time::onValueChangedListener,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));

    findViewById(z10::R::id::btn_set_time_cancle)->setOnClickListener(std::bind(&Page_set_time::onButtonClicnListener,this,std::placeholders::_1));
    findViewById(z10::R::id::btn_set_time_verify)->setOnClickListener(std::bind(&Page_set_time::onButtonClicnListener,this,std::placeholders::_1));
    findViewById(z10::R::id::title_back)->setOnClickListener(std::bind(&Page_set_time::onButtonClicnListener,this,std::placeholders::_1));
}

Page_set_time::~Page_set_time(){
}

void Page_set_time::onButtonClicnListener(View&v){
    switch (v.getId()){
    case z10::R::id::btn_set_time_cancle:
    case z10::R::id::title_back:
        this->close();
        break;
    case z10::R::id::btn_set_time_verify:
        timeSet(mPickerYear->getValue(),
                mPickerMon->getValue(),
                mPickerDay->getValue(),
                mPickeHour->getValue(),
                mPickeMin->getValue(),
                getTodayTimeSec());
        this->close();
        break;

    default:
        break;
    }
}

void Page_set_time::onValueChangedListener(NumberPicker& picker,int num_old,int num_new){
    switch(picker.getId()){
        case z10::R::id::numberpicker_set_year:
        case z10::R::id::numberpicker_set_mon:
            setMaxDay(mPickerYear->getValue(),mPickerMon->getValue());  
            break;
    }
}

//通过年 月，来设置 最大的日期（如 2月份只有29或28天）
void Page_set_time::setMaxDay(int year,int mon){
    std::tm timeinfo = {0};
    timeinfo.tm_year = year - 1900; // 年份从1900开始
    timeinfo.tm_mon = mon - 1; // 月份从0开始，所以减去1

    std::time_t time = std::mktime(&timeinfo);

    int maxDays = std::localtime(&time)->tm_mday;
    mPickerDay->setMaxValue(maxDays);
}
