#include <page_unlock.h>

Page_Unlock* Page_Unlock::mins = nullptr;

Page_Unlock::Page_Unlock():Window(0,0,-1,-1){

    LinearLayout *page = (LinearLayout *)LayoutInflater::from(getContext())->inflate("@layout/page_unlock",this);
    LOGD("Create PageUnlock");

    SeekBar*bar=(SeekBar *)findViewById(z10::R::id::unlock_progressBar);
    mRunnable = [this,bar](){
        bar->setProgress(0);
    };
    bar->setOnSeekBarChangeListener(std::bind(&Page_Unlock::onSeekBarChange,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3));
}


Page_Unlock::~Page_Unlock(){}

void Page_Unlock::onSeekBarChange(cdroid::AbsSeekBar& seekbar, int progress, bool fromUser){
    if((static_cast<float>(progress) / seekbar.getMax()) > 0.9 )
    {
        LOGD("close ScreenSaver");
        this->close();
        InputEventSource::getInstance().closeScreenSaver();
        mins = nullptr;
    }
    seekbar.removeCallbacks(mRunnable);
    seekbar.postDelayed(mRunnable,1000);
}

bool Page_Unlock::isPresent(){
    if(mins == nullptr)
        return false;
    else 
        return true;
}
