#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

#include "mode.h"
#include <homewindow.h>

ModeEditWindow::ModeEditWindow(ModeAdapter* adapter, int position, ModeWindow* container) : Window(0, 0, -1, -1) {
    modeView = container;
    defaultSelect = 1;
    setCache(position);
    LayoutInflater::from(getContext())->inflate("@layout/mode_edit", this);

    auto vClick = std::bind(&ModeEditWindow::ViewClick, this, std::placeholders::_1);

    ImageView* home = (ImageView*)findViewById(z10::R::id::mode_edit_home);
    home->setOnClickListener([this](View& view)mutable {
        modeView->closesetting();
        this->close();
        });

    ImageView* edit_img = (ImageView*)findViewById(z10::R::id::mode_edit_img);
    edit_img->setBackgroundResource(modeCache.picture);
    edit_img->setOnClickListener([this, edit_img](View& view) {
        if (defaultSelect == 6)defaultSelect = 0;
        edit_img->setBackgroundResource(modeImgMap.at(defaultSelect));
        modeCache.picture = modeImgMap.at(defaultSelect);
        modeCache.picture_2 = modeImgMap2.at(defaultSelect);
        defaultSelect++;
        });

    TextView* tv = (TextView*)findViewById(z10::R::id::mode_edit_name);
    tv->setText(modeCache.item);

    if (modeData.at(position).type == MODE_TYPE_UER) {
        auto rename = std::bind(&ModeEditWindow::ModeRename, this, std::placeholders::_1);
        tv->setOnClickListener(rename);
    }

    tv = (TextView*)findViewById(z10::R::id::mode_edit_warm_text);
    tv->setText(std::to_string(modeCache.wendu) + "℃");
    tv = (TextView*)findViewById(z10::R::id::mode_edit_water_text);
    tv->setText(std::to_string(modeCache.chushuiliang) + "mL");

    RelativeLayout* title_back = (RelativeLayout*)findViewById(z10::R::id::title_back);
    title_back->setOnClickListener([this](View& view)mutable {
        LOGD("窗口即将关闭");
        this->close();
        });

    // 温度
    ImageView* warmDown = (ImageView*)findViewById(z10::R::id::mode_edit_warm_down);
    ImageView* warmUp = (ImageView*)findViewById(z10::R::id::mode_edit_warm_up);
    if (modeData.at(position).type < MODE_TYPE_HOT_WATER) {
        warmDown->setVisibility(View::INVISIBLE);
        warmUp->setVisibility(View::INVISIBLE);
    } else {
        tv = (TextView*)findViewById(z10::R::id::mode_edit_warm_text);
        warmDown->setOnClickListener([this, tv, warmDown, warmUp](View& view)mutable {
            LOGD("温度减");
            modeCache.wendu -= 5;
            if (modeCache.wendu <= 45) {
                modeCache.wendu = 45;
                warmDown->setVisibility(View::INVISIBLE);
            }
            tv->setText(std::to_string(modeCache.wendu) + "℃");
            warmUp->setVisibility(View::VISIBLE);
            });
        warmUp->setOnClickListener([this, tv, warmUp, warmDown](View& view)mutable {
            LOGD("温度加");
            modeCache.wendu += 5;
            if (modeCache.wendu >= 100) {
                modeCache.wendu = 100;
                warmUp->setVisibility(View::INVISIBLE);
            }
            tv->setText(std::to_string(modeCache.wendu) + "℃");
            warmDown->setVisibility(View::VISIBLE);
            });
    }

    // 水量
    ImageView* waterDown = (ImageView*)findViewById(z10::R::id::mode_edit_water_down);
    ImageView* waterUp = (ImageView*)findViewById(z10::R::id::mode_edit_water_up);
    tv = (TextView*)findViewById(z10::R::id::mode_edit_water_text);
    waterDown->setOnClickListener([this, tv, waterDown, waterUp](View& view)mutable {
        LOGD("水量减");
        modeCache.chushuiliang -= 50;
        if (modeCache.chushuiliang <= 150) {
            modeCache.chushuiliang = 150;
            waterDown->setVisibility(View::INVISIBLE);
        }
        tv->setText(std::to_string(modeCache.chushuiliang) + "mL");
        waterUp->setVisibility(View::VISIBLE);
        });
    waterUp->setOnClickListener([this, tv, waterUp, waterDown](View& view)mutable {
        LOGD("水量加");
        modeCache.chushuiliang += 50;
        if (modeCache.chushuiliang >= 1000) {
            modeCache.chushuiliang = 1000;
            waterUp->setVisibility(View::INVISIBLE);
        }
        tv->setText(std::to_string(modeCache.chushuiliang) + "mL");
        waterDown->setVisibility(View::VISIBLE);
        });

    // 保存
    tv = (TextView*)findViewById(z10::R::id::mode_edit_enter);
    tv->setOnClickListener([this, adapter, position](View& view) mutable {
        adapter->updateMode(position, modeCache);
        adapter->notifyDataSetChanged();
        LOGD("保存成功");
        this->close();
        }
    );

    // 删除
    ImageView* button = (ImageView*)findViewById(z10::R::id::mode_edit_del);
    if (modeData.at(position).type < MODE_TYPE_UER) {
        button->setVisibility(View::GONE);
    } else {
        button->setOnClickListener([this, adapter, position](View& view)mutable {
            delModeWindow* popdel = new delModeWindow();

            TextView* tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_title);
            tv->setText("确认删除“" + modeCache.item + "”模式？");
            tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_warm);
            tv->setText(std::to_string(modeCache.wendu) + "℃");
            tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_water);
            tv->setText(std::to_string(modeCache.chushuiliang) + "mL");

            tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_enter);
            tv->setOnClickListener([this, popdel, adapter, position](View& view) {
                if (!modeData.at(position).type) {
                    popdel->close();
                    return;
                }
                adapter->delMode(position);
                LOGD("%d数据已删除", position);
                popdel->close();
                this->close();
                });
            });
    }
}

// 事件点击监听
void ModeEditWindow::ViewClick(View& view) {
    LOGD("click %d", view.getId());
    switch (view.getId()) {
    case z10::R::id::mode_back:
        LOGD("窗口即将关闭");
        // this->close();
        break;

    case z10::R::id::mode_edit_back:

    default:
        break;
    }
}

void ModeEditWindow::setCache(int position) {
    modeCache.picture_2 = modeData.at(position).picture_2;
    modeCache.picture = modeData.at(position).picture;
    modeCache.item = modeData.at(position).item;
    modeCache.wendu = modeData.at(position).wendu;
    modeCache.chushuiliang = modeData.at(position).chushuiliang;
    modeCache.type = modeData.at(position).type;
    LOGD("%s----%d----%d----%d", modeCache.item, modeCache.wendu, modeCache.chushuiliang, modeCache.type);
}


ModeEditWindow::~ModeEditWindow() {
}

void ModeEditWindow::ModeRename(View& view) {
    KInput* rename = new KInput(modeCache.item, "请输入该自定义出水模式的名称", 6, "@mipmap/intput_bg_add");

    TextView* inputEnter = (TextView*)rename->findViewById(z10::R::id::input_enter);
    inputEnter->setOnClickListener([this, rename](View& view) {
        this->setCacheName(rename->getInput());
        rename->close();
        });
}

void ModeEditWindow::setCacheName(std::string name) {
    TextView* tv = (TextView*)findViewById(z10::R::id::mode_edit_name);
    tv->setText(name);
    modeCache.item = name;
}
