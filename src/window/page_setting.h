#pragma once
#include <widget/cdwindow.h>

#include <homewindow.h>

class Page_setting : public Window
{
private:
    Switch *mSwCool;
    Switch *mSwIce;
    Switch *mSwLock;

protected:
    void onSettingBoxClick(View &);
    void onSettingSwClick(View &);

public:
    Page_setting();
    ~Page_setting();
};
