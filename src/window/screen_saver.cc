#include <screen_saver.h>
#include <homewindow.h>

Screen_saver::Screen_saver() :Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/screen_saver", this);
    LOGD("Create ScreenSaver");

    findViewById(z10::R::id::linearLayout)->setOnClickListener([this](View& view) { 
        this->close();
        InputEventSource::getInstance().closeScreenSaver();
    });
}

Screen_saver::~Screen_saver() { }

