#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>

#include <homewindow.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

// 弹窗中 需要开启的功能
typedef enum Change_Func {
    OPEN_FUNC_COOL,
    OPEN_FUNC_ICE,
    OPEN_FUNC_COOL_ICE,
    CLOSE_FUNC_COOL,
    CLOSE_FUNC_ICE,
    CLOSE_FUNC_COOL_ICE
}Func;

typedef enum Input_Func {
    INPUT_FUNC_COOL,
    INPUT_FUNC_ICE,
    
    INPUT_FUNC_COOL_ING,
    INPUT_FUNC_ICE_ING
}InFunc;


class Popwindow_text_hint{
private:
    TextView *mTextView;
    TextView *mBtnCancel;
    TextView *mBtnEnter;

    ViewGroup *mViewGroup;
    ViewGroup *mParentViewGroup;

    int mFunFlag;

    std::string mText;
    std::string mCancel_Text;
    std::string mEnter_Text;

    void initState(int func);
    void onClickBackListener(View&v);
public:
    Popwindow_text_hint(ViewGroup *parent, int func,std::string text,std::string enter = "确定",std::string cancel = "取消");
    Popwindow_text_hint(ViewGroup *parent, int func);
    ~Popwindow_text_hint();
};


class Popwindow_tip_hint{
private:
    ViewGroup *mViewGroup;
    ViewGroup *mParentViewGroup;

    void onClickBackListener(View&v);
public:
    Popwindow_tip_hint(ViewGroup *parent, int func);
    ~Popwindow_tip_hint();
};
