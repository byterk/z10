﻿
#include "wind_mgr.h"

#include <conn_mgr.h>
#include <homewindow.h>

#include "page_item.h"

#define LOAD_WINDOW(T, I)                                                                                              \
    T *wnd;                                                                                                            \
    do {                                                                                                               \
        wnd = __dc(T, getWindow(I));                                                                                   \
        if (wnd) {                                                                                                     \
            LOG(WARN) << "window already exists, maybe reflush UI. ID=" << I;                                          \
            break;                                                                                                     \
        }                                                                                                              \
        LOG(INFO) << "new window. ID=" << I;                                                                           \
        wnd = new T();                                                                                                 \
        wnd->setId(I);                                                                                                 \
        regWindow(I, wnd);                                                                                             \
    } while (false)

CWindMgr *g_objWindMgr = CWindMgr::ins();

void CWindMgr::showMain() {
}

void CWindMgr::closeWindow(int windID) {
}

bool CWindMgr::exists(emWindowID ID) {
    return getWindow(ID) != 0;
}

bool CWindMgr::existsClose(emWindowID ID) {
}

void CWindMgr::closeOther(emWindowID Id) {
}

void CWindMgr::closeOtherEx(const std::set<int> &Id) {
}

void CWindMgr::check() {
}

void CWindMgr::showWind(emWindowID ID) {
}

void CWindMgr::statusNotify(int value /* = 0*/) {
}
