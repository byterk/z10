#pragma once
#include <widget/cdwindow.h>
#include <homewindow.h>

class ScreenLightWindow : public Window
{
private:
   SeekBar* mLightSeekbar;
protected:
   void onClickBackListener(View &);
public:
   ScreenLightWindow();
   ~ScreenLightWindow();
};
