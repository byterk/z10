#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

#include "mode.h"
#include <homewindow.h>

bool isChangeModeData = false;

ModeWindow::ModeWindow(Window* container) :Window(0, 0, -1, -1) {
    settingView = container;
    LayoutInflater::from(getContext())->inflate("@layout/setting_mode", this);

    ImageView* home = (ImageView*)findViewById(z10::R::id::setting_mode_home);
    home->setOnClickListener([this](View& view)mutable {this->closesetting();});

    ModeAdapter* adapter = new ModeAdapter(this);
    GridView* lv = (GridView*)findViewById(z10::R::id::setting_mode_listView);
    lv->setVerticalScrollBarEnabled(true);

    lv->setAdapter(adapter);
    adapter->notifyDataSetChanged();

    TextView* tv = (TextView*)findViewById(z10::R::id::setting_modeadd);
    tv->setOnClickListener([this, adapter](View& view) mutable {adapter->buildadd(); });

    RelativeLayout* title_back = (RelativeLayout*)this->findViewById(z10::R::id::title_back);
    title_back->setOnClickListener([this](View& view)mutable {
        this->close();
        if (isChangeModeData) HomeWindow::ins()->notifyDataChanged();
        });
}

// 初始化数据
ModeAdapter::ModeAdapter(ModeWindow* container) {
    thisContainer = container;
}

// 构建页面
View* ModeAdapter::getView(int position, View* convertView, ViewGroup* parent) {
    position += skipdata;
    View* view = LayoutInflater::from(parent->getContext())->inflate("@layout/setting_mode_list", nullptr, false);


    // 设置背景图
    ImageView* mode_img = (ImageView*)view->findViewById(z10::R::id::mode_img);
    mode_img->setBackgroundResource(modeData.at(position).picture_2);
    mode_img->setOnClickListener([this, position](View& view) mutable { buildedit(position); });

    // 设置内容
    TextView* name = (TextView*)view->findViewById(z10::R::id::mode_name);
    TextView* warm = (TextView*)view->findViewById(z10::R::id::mode_warm);
    TextView* water = (TextView*)view->findViewById(z10::R::id::mode_water);
    name->setText(modeData.at(position).item);
    warm->setText(std::to_string(modeData.at(position).wendu) + "℃");
    water->setText(std::to_string(modeData.at(position).chushuiliang) + "mL");

    // modeData
    ImageView* mode_del = (ImageView*)view->findViewById(z10::R::id::mode_del);
    if (modeData.at(position).type < MODE_TYPE_UER) {
        mode_del->setVisibility(View::INVISIBLE);
    } else {
        mode_del->setOnClickListener([this, position](View& view) mutable {DelPop(position);});
    };

    // 左移按钮
    ImageView* turnleft = (ImageView*)view->findViewById(z10::R::id::mode_turnleft);
    turnleft->setBackgroundResource(position - skipdata == 0 ? "@mipmap/icon_noleft" : "@mipmap/icon_left");
    turnleft->setOnClickListener([this, position](View& view) mutable {changedata(position, 0); });

    // 右移按钮
    ImageView* turnright = (ImageView*)view->findViewById(z10::R::id::mode_turnright);
    turnright->setBackgroundResource(position == modeData.size() - 1 ? "@mipmap/icon_noright" : "@mipmap/icon_right");
    turnright->setOnClickListener([this, position](View& view) mutable {changedata(position, 1); });

    return view;
}

void ModeAdapter::changedata(int location, int dir) {
    LOGD("交换%d到%s", location, dir ? "右边" : "左边");
    if (dir && location >= skipdata && location < modeData.size() - 1) {
        std::swap(modeData[location], modeData[location + 1]);
    } else if (!dir && location > skipdata && location < modeData.size()) {
        std::swap(modeData[location], modeData[location - 1]);
    } else {
        LOGD("%d无法%s | %d", location, dir ? "右移" : "左移", dir);
        return;
    }
    LOGD("交换成功");
    isChangeModeData = true;
    // HomeWindow::ins()->notifyDataChanged();
    notifyDataSetChanged();
}

void ModeAdapter::delMode(int location) {
    LOGD("++++++++++++++++++++++++++++");
    for (int i = 0;i < modeData.size();i++) {
        LOGD("NUM:%d -- %d", i, modeData.at(i).type);
    };
    LOGD("+++++%d++++++", location);

    if (!!modeData.at(location).type && location >= 0 && location < modeData.size()) {
        modeData.erase(modeData.begin() + location);
        LOGD("删除第%d项成功", location);
        isChangeModeData = true;
        // HomeWindow::ins()->notifyDataChanged();
        notifyDataSetChanged();
    } else { LOGD("删除第%d项失败", location); }

    LOGD("++++++++++++++++++++++++++++");
    for (int i = 0;i < modeData.size();i++) {
        LOGD("NUM:%d -- %d", i, modeData.at(i).type);
    };
}

// 删除弹窗
void ModeAdapter::DelPop(int position) {
    delModeWindow* popdel = new delModeWindow();

    // 设置文本
    TextView* tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_title);
    tv->setText("确认删除“" + modeData.at(position).item + "”模式？");
    tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_warm);
    tv->setText(std::to_string(modeData.at(position).wendu) + "℃");
    tv = (TextView*)popdel->findViewById(z10::R::id::mode_del_water);
    tv->setText(std::to_string(modeData.at(position).chushuiliang) + "mL");

    // 设置按钮
    TextView* canncel = (TextView*)popdel->findViewById(z10::R::id::mode_del_cancel);
    canncel->setOnClickListener([this, popdel](View& view) {popdel->close();});
    TextView* enter = (TextView*)popdel->findViewById(z10::R::id::mode_del_enter);
    enter->setOnClickListener([this, popdel, position](View& view) {
        if (!modeData.at(position).type) {
            popdel->close();
            return;
        }
        delMode(position);
        LOGD("第%d:%d数据已删除", position, modeData.size());
        popdel->close();
        });



    LinearLayout* linear = (LinearLayout*)popdel->findViewById(z10::R::id::linearLayout);            //截取 文本的点击事件 
    TextView* text_view = (TextView*)popdel->findViewById(z10::R::id::mode_del_Box);
    linear->setOnClickListener([this, popdel](View& view) { popdel->close();});        //点解 空白地方返回上一级
    text_view->setOnClickListener([this](View& view) { });                      //截取 文本的点击事件  不做操作
}

void ModeWindow::ModeWindowClick(View& view) {
    LOGD("click %d", view.getId());
    switch (view.getId()) {
    case z10::R::id::mode_back:
        LOGD("窗口即将关闭");
        this->close();
        break;
    default:
        break;
    }
}

ModeWindow::~ModeWindow() {
}

void ModeWindow::closesetting() {
    LOGD("正在关闭设置界面");
    if (isChangeModeData) HomeWindow::ins()->notifyDataChanged();
    settingView->close();
    this->close();
}

// 跳转编辑/增加页面
void ModeAdapter::buildedit(int position) {
    Window* editEdit = new ModeEditWindow(this, position, thisContainer);
};
void ModeAdapter::buildadd() {
    if (modeData.size() < 14) {
        Window* modeAdd = new ModeAddWindow(this, thisContainer);
    }
};

delModeWindow::delModeWindow() : Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/popwindow_modedel_hint", this);
    TextView* tv = (TextView*)findViewById(z10::R::id::mode_del_cancel);
    tv->setOnClickListener([this](View& view) {
        LOGD("页面已关闭");
        this->close();
        });
}

void ModeAdapter::addMode(WATER cache) {
    modeData.push_back(cache);
    isChangeModeData = true;
    // HomeWindow::ins()->notifyDataChanged();
    notifyDataSetChanged();
};

void ModeAdapter::updateMode(int location, WATER cache) {
    modeData.at(location).picture_2 = cache.picture_2;
    modeData.at(location).picture = cache.picture;
    modeData.at(location).item = cache.item;
    modeData.at(location).wendu = cache.wendu;
    modeData.at(location).chushuiliang = cache.chushuiliang;
    modeData.at(location).type = cache.type;
    isChangeModeData = true;
    // HomeWindow::ins()->notifyDataChanged();
    notifyDataSetChanged();
};