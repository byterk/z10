#include <page_uv.h>
#include <homewindow.h>

Page_uv::Page_uv():Window(0,0,-1,-1){
    LayoutInflater::from(getContext())->inflate("@layout/page_uvshajun",this);
    //得到 numberpicker 的控件指针
    mPickeStarHour = (NumberPicker *)findViewById(z10::R::id::numberpicker_star_hour);
    mPickeStarMin = (NumberPicker *)findViewById(z10::R::id::numberpicker_star_min);
    mPickeEndHour = (NumberPicker *)findViewById(z10::R::id::numberpicker_end_hour);
    mPickeEndMin = (NumberPicker *)findViewById(z10::R::id::numberpicker_end_min);
    //得到 checkbox 的控件指针
    mCheckBoxKeepOpen = (CheckBox *)findViewById(z10::R::id::checkbox_keep_uv_mode);
    mCheckBoxTimeOpen = (CheckBox *)findViewById(z10::R::id::checkbox_set_uv_time);
    mCheckBoxInitativeOpen = (CheckBox *)findViewById(z10::R::id::checkbox_initiative_uv);
    //得到 btn 的控件指针 ，立即杀菌
    mBtnNowShajun = (Button *)findViewById(z10::R::id::btn_nowshajun);

    mCheckBoxKeepOpen->setOnCheckedChangeListener(std::bind(&Page_uv::onCheckedChangeListener,this,std::placeholders::_1,std::placeholders::_2));
    mCheckBoxTimeOpen->setOnCheckedChangeListener(std::bind(&Page_uv::onCheckedChangeListener,this,std::placeholders::_1,std::placeholders::_2));
    mCheckBoxInitativeOpen->setOnCheckedChangeListener(std::bind(&Page_uv::onCheckedChangeListener,this,std::placeholders::_1,std::placeholders::_2));

    mPickeStarHour->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeStarMin->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeEndHour->setFormatter([this](int value){ return fillLength(value, 2); });
    mPickeEndMin->setFormatter([this](int value){ return fillLength(value, 2); });

    mBtnNowShajun->setOnClickListener(std::bind(&Page_uv::onClickBackListener,this,std::placeholders::_1));
    findViewById(z10::R::id::title_back)->setOnClickListener(std::bind(&Page_uv::onClickBackListener,this,std::placeholders::_1));
    findViewById(z10::R::id::btn_keep)->setOnClickListener(std::bind(&Page_uv::onClickBackListener,this,std::placeholders::_1));
    findViewById(z10::R::id::checkbox_set_uv_mode_layout)->setOnClickListener(std::bind(&Page_uv::onClickBackListener,this,std::placeholders::_1));
    findViewById(z10::R::id::checkbox_set_uv_time_layout)->setOnClickListener(std::bind(&Page_uv::onClickBackListener,this,std::placeholders::_1));
    findViewById(z10::R::id::checkbox_initiative_uv_layout)->setOnClickListener(std::bind(&Page_uv::onClickBackListener,this,std::placeholders::_1));

    auto funValueChange = std::bind(&Page_uv::onNumPickerValueChange,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3);
    mPickeStarHour->setOnValueChangedListener(funValueChange);
    mPickeStarMin->setOnValueChangedListener(funValueChange);
    mPickeEndHour->setOnValueChangedListener(funValueChange);
    mPickeEndMin->setOnValueChangedListener(funValueChange);

    //初始化 checkbox 的状态，以及立即杀菌按钮的状态
    mBtnNowShajun->setEnabled(false);
    if(g_appData.UVshajun == PRO_STATE_UV_KEEP_ENABLED){
        mCheckBoxKeepOpen->setChecked(true);
        mCheckBoxTimeOpen->setChecked(false);
        mCheckBoxInitativeOpen->setChecked(false);
    }
    else if(g_appData.UVshajun == PRO_STATE_UV_RESERVATION){
        mCheckBoxKeepOpen->setChecked(false);
        mCheckBoxTimeOpen->setChecked(true);
        mCheckBoxInitativeOpen->setChecked(false);
    }
    else if(g_appData.UVshajun == PRO_STATE_UV_USER){
        mCheckBoxKeepOpen->setChecked(false);
        mCheckBoxTimeOpen->setChecked(false);
        mCheckBoxInitativeOpen->setChecked(true);
        mBtnNowShajun->setEnabled(true);
    }
    else if(g_appData.UVshajun == PRO_STATE_UV_NONE){
        mCheckBoxKeepOpen->setChecked(false);
        mCheckBoxTimeOpen->setChecked(false);
        mCheckBoxInitativeOpen->setChecked(false);
    }
}

Page_uv::~Page_uv(){
}

void Page_uv::onClickBackListener(View&v){
    switch(v.getId()){
        case z10::R::id::title_back:{
            LOG(VERBOSE) << "close page_uvshajun";
            this->close();
            break;
        }
        case z10::R::id::btn_keep:{
            g_appData.star_time_uv.tm_hour  = mPickeStarHour->getValue();
            g_appData.star_time_uv.tm_min   = mPickeStarMin->getValue();
            g_appData.end_time_uv.tm_hour   = mPickeEndHour->getValue();
            g_appData.end_time_uv.tm_min    = mPickeEndMin->getValue();

            LOGI("uv杀菌的预约时间为   %d:%d - %d:%d",g_appData.star_time_uv.tm_hour,
                                                    g_appData.star_time_uv.tm_min, 
                                                    g_appData.end_time_uv.tm_hour,
                                                    g_appData.end_time_uv.tm_min);
            if(mCheckBoxKeepOpen->isChecked()){
                g_appData.UVshajun = PRO_STATE_UV_KEEP_ENABLED;
            }
            else if(mCheckBoxTimeOpen->isChecked()){
                g_appData.UVshajun = PRO_STATE_UV_RESERVATION;
            }
            else if(mCheckBoxInitativeOpen->isChecked()){
                g_appData.UVshajun = PRO_STATE_UV_USER;
            }
            else{
                g_appData.UVshajun = PRO_STATE_UV_NONE;
            }
            g_appData.isUVshajun = isTimeRange(g_appData.star_time_uv, g_appData.end_time_uv); 
            Activity_header::ins()->changeState((State)g_appData.UVshajun);
            this->close();    
            break;
        }
        case z10::R::id::btn_nowshajun:{
            LOG(VERBOSE) << "onClickBackListener btn_nowshajun";
            break;
        }
        case z10::R::id::checkbox_set_uv_mode_layout:{
            mCheckBoxKeepOpen->setChecked(!mCheckBoxKeepOpen->isChecked());
            break;
        }
        case z10::R::id::checkbox_set_uv_time_layout:{
            mCheckBoxTimeOpen->setChecked(!mCheckBoxTimeOpen->isChecked());
            break;
        }
        case z10::R::id::checkbox_initiative_uv_layout:{
            mCheckBoxInitativeOpen->setChecked(!mCheckBoxInitativeOpen->isChecked());
            break;
        }
    }
}

void Page_uv::onCheckedChangeListener(CompoundButton&view,bool isChecked){
    switch(view.getId()){
        case z10::R::id::checkbox_keep_uv_mode:{
            LOG(VERBOSE) << "onCheck keep uv = " <<  isChecked ? "true":"false";
            if(isChecked){
                mCheckBoxTimeOpen->setChecked(false);
                mCheckBoxInitativeOpen->setChecked(false);
                mBtnNowShajun->setEnabled(false);
            }
            break;
        }
        case z10::R::id::checkbox_set_uv_time:{
            LOG(VERBOSE) << "onCheck set time uv = " <<  isChecked ? "true":"false";
            if(isChecked){
                mCheckBoxKeepOpen->setChecked(false);
                mCheckBoxInitativeOpen->setChecked(false);
                mBtnNowShajun->setEnabled(false);
            }
            break;
        }
        case z10::R::id::checkbox_initiative_uv:{
            LOG(VERBOSE) << "onCheck initiativ uv = " <<  isChecked ? "true":"false";
            if(isChecked){
                mCheckBoxKeepOpen->setChecked(false);
                mCheckBoxTimeOpen->setChecked(false);
                mBtnNowShajun->setEnabled(true);
                break;
            }
            else{
                mBtnNowShajun->setEnabled(false);
                break;
            }
        }
    }
}

void Page_uv::onNumPickerValueChange(NumberPicker &npx, int index, int value){
    if(!mCheckBoxTimeOpen->isChecked()){
        mCheckBoxTimeOpen->setChecked(true);
    }
}
