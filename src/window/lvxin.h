#pragma once
#include <widget/cdwindow.h>

#include <arc_seekbar.h>

enum{
    RESET_NONE = 1234,
    RESET_PP_ING,
    RESET_RO_ING,
    RESET_PP_DOWM,
    RESET_RO_DOWM
};

class LvXinWindow :public Window {
private:
    Runnable mRunnable;

    TextView* totalWaterText;
    TextView* prueWaterText;
    TextView* cleanWaterText;

    ArcSeekBar* arc_HPCC;
    ArcSeekBar* arc_RO;
    TextView* arc_HPCC_text;
    TextView* arc_RO_text;

    void resetLvxin(int value);
    void buttonClick(View&view);
protected:
    int resetPPStatus = RESET_NONE;
    int resetROStatus = RESET_NONE;
public:
    LvXinWindow();
    ~LvXinWindow();

    
};
