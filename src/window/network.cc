#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

#include "network.h"
#include <homewindow.h>

// 强信号：Quality > 75% 且 Signal level > -50 dBm
// 中等信号：Quality > 50% 且 Signal level > -60 dBm
// 弱信号：Quality > 25% 且 Signal level > -70 dBm
// 非常弱信号：Quality < 25% 或 Signal level < -70 dBm

NetWorkWindow::NetWorkWindow() : Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/network", this);

    RelativeLayout* title_back = (RelativeLayout*)findViewById(z10::R::id::title_back);
    title_back->setOnClickListener([this](View& view) {this->close();});


    auto func = std::bind(&NetWorkWindow::toBlink, this, std::placeholders::_1);
    TextView* blink = (TextView*)findViewById(z10::R::id::network_blink);
    blink->setOnClickListener(func);
    setBlinkStatus(false);

    MyAdapter* adapter = new MyAdapter(this);
    ListView* lv = (ListView*)findViewById(z10::R::id::setting_network_listView);
    lv->setVerticalScrollBarEnabled(true);

    getConnectInfo(this, lv);



    lv->setAdapter(adapter);
    adapter->notifyDataSetChanged();
    lv->setOnItemClickListener([ ](AdapterView& lv, View& v, int pos, long id) { LOGD("clicked %d", pos); });
}

void NetWorkWindow::getConnectInfo(ViewGroup* container, ListView* listview) {
    std::string now_status = container->getContext()->getString("@string/network_now_status");
    wlanConnect.status = std::stoi(now_status);

    // 已连接
    if (wlanConnect.status == 0 && HEADERVIEW != nullptr) {
        listview->removeView(HEADERVIEW);
        HEADERVIEW = nullptr;
        return;
    } else if (wlanConnect.status == 0) {
        return;
    }
    std::string now_signal = container->getContext()->getString("@string/network_now_signal");
    std::string now_name = container->getContext()->getString("@string/network_now_name");
    std::string now_lock = container->getContext()->getString("@string/network_now_lock");

    setConnectInfo(std::stoi(now_signal), now_name, std::stoi(now_lock));
    createConnect(listview);
}

void NetWorkWindow::createConnect(ListView* listview) {
    View* view = LayoutInflater::from(this->getContext())->inflate("@layout/network_now", nullptr, false);

    ImageView* icon = (ImageView*)view->findViewById(z10::R::id::wifi_icon);
    icon->setImageLevel(wlanConnect.detail.signal);
    // icon->setActivated(true);

    TextView* name = (TextView*)view->findViewById(z10::R::id::wifi_name);
    name->setText(wlanConnect.detail.name);

    icon->setEnabled(!!wlanConnect.detail.lock);

    listview->addHeaderView(view, nullptr, false);
    HEADERVIEW = view;
}



void NetWorkWindow::toBlink(View& view) {
    if (BLINKSTATUS) {
        showUnBlink();
    } else {
        Window* qr = new Window(0, 0, -1, -1);
        LayoutInflater::from(getContext())->inflate("@layout/qr_code", qr);
        TextView* q_qr = (TextView*)qr->findViewById(z10::R::id::qr_hint_cancel);
        setBlinkStatus(true);
        q_qr->setOnClickListener([qr](View& view) {qr->close();});

        Runnable qrRunnable;
        qrRunnable = [this, qr]() {
            this->showSuccess();
            qr->close();
            };
        postDelayed(qrRunnable, 2000);
    }
}

void NetWorkWindow::setBlinkStatus(bool status) {
    BLINKSTATUS = status;
    TextView* blink = (TextView*)findViewById(z10::R::id::network_blink);
    blink->setText(status ? "取消绑定" : "设备绑定");
    blink->setBackgroundResource(status ? "@drawable/btn_1affffff_r25" : "@drawable/setting_btn_blue_r30");

    TextView* blink_text = (TextView*)findViewById(z10::R::id::blink_text);
    blink_text->setText(status ? "已绑定设备" : "");

}

void NetWorkWindow::showSuccess() {
    Window* sc = new Window(0, 0, -1, -1);
    LayoutInflater::from(getContext())->inflate("@layout/popwindow_blink_success", sc);
    TextView* q_sc = (TextView*)sc->findViewById(z10::R::id::popwindow_success_hint_enter);
    q_sc->setOnClickListener([this, sc](View& view) {
        sc->close();
        this->setBlinkStatus(true);
        });
}

void NetWorkWindow::showUnBlink() {
    Window* sc = new Window(0, 0, -1, -1);
    LayoutInflater::from(getContext())->inflate("@layout/popwindow_text_hint", sc);

    TextView* text_view = (TextView*)sc->findViewById(z10::R::id::popwindow_hint_text);
    TextView* text_cancel = (TextView*)sc->findViewById(z10::R::id::popwindow_hint_cancel);
    TextView* text_enter = (TextView*)sc->findViewById(z10::R::id::popwindow_hint_enter);

    text_view->setText("请点击解绑按钮进行解绑， 或通过手机APP进行设备解绑;");
    text_view->setLineHeight(45);
    text_cancel->setText("取消");
    text_enter->setText("解绑");

    text_cancel->setOnClickListener([this, sc](View& view) {sc->close();});
    text_enter->setOnClickListener([this, sc](View& view) {
        LOGD("6666");
        this->setBlinkStatus(false);
        sc->close();
        });

}


NetWorkWindow::~NetWorkWindow() {
    LOGD("退出网络设置");
    // delete anim;
}

MyAdapter::MyAdapter(ViewGroup* container) {

    std::vector<int> signal;
    std::vector<std::string> name;
    std::vector<int> lock;

    container->getContext()->getArray("@array/network_list_signal", signal);
    container->getContext()->getArray("@array/network_list_name", name);
    container->getContext()->getArray("@array/network_list_lock", lock);

    LOGD("detanum: %d", signal.size());

    for (int i = 0; i < signal.size(); i++) {
        wlanList.push_back({ signal.at(i), name.at(i), lock.at(i) });
    };
}

int MyAdapter::getCount() const {
    return wlanList.size();
}

View* MyAdapter::getView(int position, View* convertView, ViewGroup* parent) {

    View* view = LayoutInflater::from(parent->getContext())->inflate("@layout/network_list", nullptr, false);

    // 网络名
    TextView* tv = (TextView*)view->findViewById(z10::R::id::network_list_name);
    tv->setText(wlanList.at(position).name);

    // 信号图标
    ImageView* icon = (ImageView*)view->findViewById(z10::R::id::wifi_icon);
    icon->setImageLevel(wlanList.at(position).signal);

    icon->setEnabled(false);

    // icon->setLayerType(View::LAYER_TYPE_SOFTWARE);
    // icon->setAlpha(0.2f);

    // 显示网络名
    icon = (ImageView*)view->findViewById(z10::R::id::wifi_lock);
    icon->setEnabled(!!wlanList.at(position).lock);

    LOGD("%s-%d-%d", wlanList.at(position).name.c_str(), wlanList.at(position).signal, wlanList.at(position).lock);

    // parent->addView(view);
    return view;
}

void* MyAdapter::getItem(int position) const {
    return nullptr;
}
