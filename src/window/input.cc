#include "input.h"

KInput::KInput(std::string iText, std::string description, int maxNum, std::string background) : Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/input", this);
    textDes = description;
    textMaxNum = maxNum;
    inputBg = background;

    LinearLayout* ly = (LinearLayout*)findViewById(z10::R::id::linearLayout);
    ly->setBackgroundResource(inputBg);
    listenClick();

    input = (TextView*)findViewById(z10::R::id::text_input);
    inputDes = (TextView*)findViewById(z10::R::id::text_input_description);
    iText.size() == 0 ? inputDes->setText(textDes) : input->setText(iText);

    keyboard_1 = (LinearLayout*)findViewById(z10::R::id::keyboard_1);
    keyboard_2 = (LinearLayout*)findViewById(z10::R::id::keyboard_2);
    keyboard_3 = (LinearLayout*)findViewById(z10::R::id::keyboard_3);
    keyboard_4 = (LinearLayout*)findViewById(z10::R::id::keyboard_4);
    for (int i = 0;i < 20;i++) imgTag.push_back(i);
    enType = false;
    fuhao = false;


    setKey();
    auto key_click_func = std::bind(&KInput::onKeyClick, this, std::placeholders::_1);
    TextView* mode = (TextView*)findViewById(z10::R::id::keyboard_mode);
    mode->setOnClickListener(key_click_func);
    TextView* space = (TextView*)findViewById(z10::R::id::keyboard_space);
    space->setOnClickListener(key_click_func);

    View* cursor = (View*)findViewById(z10::R::id::input_cursor);
    cursorstatus = true;



    cursor_blink = [this, cursor]() {
        cursor->setBackgroundResource(this->getcursorstatus() ? "#2f3542" : "#00000000");
        this->setcursorstatus(!cursorstatus);
        cursor->postDelayed(cursor_blink, 600);
        };
    cursor->post(cursor_blink);
}

View* KInput::newKey(std::string text) {
    auto key_click_func = std::bind(&KInput::onKeyClick, this, std::placeholders::_1);
    View* view = LayoutInflater::from(getContext())->inflate("@layout/keyboard_key", nullptr);
    TextView* key = (TextView*)view->findViewById(z10::R::id::keyboard_key);
    key->setText(text);
    key->setOnClickListener(key_click_func);
    return view;
}

View* KInput::newKey(int type) {
    auto key_click_func = std::bind(&KInput::onKeyImgClick, this, std::placeholders::_1);
    View* view = LayoutInflater::from(getContext())->inflate("@layout/keyboard_key", nullptr);
    ImageView* key_img = (ImageView*)view->findViewById(z10::R::id::keyboard_img);
    TextView* key = (TextView*)view->findViewById(z10::R::id::keyboard_key);
    switch (type) {
    case 1:
        key_img->setBackgroundResource("@mipmap/input_back");
        key->setTag(&imgTag.at(1));
        break;
    case 2:
        key_img->setBackgroundResource(enType ? "@mipmap/input_up" : "@mipmap/input_up_null");
        key->setTag(&imgTag.at(2));
        break;
    default:
        break;
    }
    key->setOnClickListener(key_click_func);
    return view;
}

void KInput::onKeyClick(View& view) {
    TextView* tv = dynamic_cast<TextView*>(&view);
    std::string text = tv->getText();
    if (textMaxNum != 0 && input->getText().size() >= textMaxNum)return;
    if (text == "123" || text == "+-#") {
        tv->setText(text == "123" ? "+-#" : "123");
        fuhao = !fuhao;
        setKey();
    } else if (text == "Space") {
        input->setText(input->getText() + " ");
    } else {
        input->setText(input->getText() + text);
    }
    inputDes->setText("");
    LOGD(text.c_str());
}


void KInput::onKeyImgClick(View& view) {
    TextView* iv = dynamic_cast<TextView*>(&view);
    int* tag = static_cast<int*>(iv->getTag());
    if (tag == nullptr) {
        LOGD("tag异常");
        return;
    }
    switch (*tag) {
    case 1: {
        std::string cache = input->getText();
        if (!input->getText().empty()) {
            LOGD("删除前：%s", input->getText().c_str());
            while (!cache.empty()) {
                unsigned char last_byte = cache.back();
                cache.pop_back();
                // If the last byte is ASCII or the first byte of a UTF-8 character, we're done.
                if ((last_byte & 0x80) == 0 || (last_byte & 0xC0) == 0xC0) { break; }
            }
            input->setText(cache);
            LOGD("删除后：%s", input->getText().c_str());
        } else {
            LOGD("删除失败");
        }
        cache.empty() ? inputDes->setText(textDes) : inputDes->setText("");
        break;}
    case 2:
        enType = !enType;
        setKey();
        break;
    default:
        break;
    }
}

void KInput::listenClick() {
    LinearLayout* ly = (LinearLayout*)findViewById(z10::R::id::linearLayout);
    ly->setOnClickListener([this](View& view) {
        this->close();
        });
    ly = (LinearLayout*)findViewById(z10::R::id::input_linearLayout);
    ly->setOnClickListener([this](View& view) {
        // this->close();
        });
    ly = (LinearLayout*)findViewById(z10::R::id::keyboard_linearLayout);
    ly->setOnClickListener([this](View& view) {
        // this->close();
        });
}

void KInput::setKey() {
    if (!fuhao) {
        setKey_num();
        setKey_yingwen(enType);
    } else {
        setKey_fuhao();
    }
}

void KInput::setKey_num() {
    keyboard_1->removeAllViews();
    for (int i = 0;i < 10;i++) {
        keyboard_1->addView(newKey(std::to_string(i)));
    }
}

void KInput::setKey_yingwen(bool type) {
    keyboard_2->removeAllViews();
    keyboard_3->removeAllViews();
    keyboard_4->removeAllViews();

    keyboard_4->addView(newKey(2));

    if (!type) {
        for (int i = 0;i < keyboardMap.at(0).size();i++) {
            keyboard_2->addView(newKey(keyboardMap.at(0).at(i)));
        }

        for (int i = 0;i < keyboardMap.at(1).size();i++) {
            keyboard_3->addView(newKey(keyboardMap.at(1).at(i)));
        }

        for (int i = 0;i < keyboardMap.at(2).size();i++) {
            keyboard_4->addView(newKey(keyboardMap.at(2).at(i)));
        }
    } else {
        for (int i = 0;i < keyboardMap.at(3).size();i++) {
            keyboard_2->addView(newKey(keyboardMap.at(3).at(i)));
        }

        for (int i = 0;i < keyboardMap.at(4).size();i++) {
            keyboard_3->addView(newKey(keyboardMap.at(4).at(i)));
        }

        for (int i = 0;i < keyboardMap.at(5).size();i++) {
            keyboard_4->addView(newKey(keyboardMap.at(5).at(i)));
        }
    }
    keyboard_4->addView(newKey(1));
}

void KInput::setKey_fuhao() {
    keyboard_1->removeAllViews();
    keyboard_2->removeAllViews();
    keyboard_3->removeAllViews();
    keyboard_4->removeAllViews();

    for (int i = 0;i < keyboardMap_2.at(0).size();i++) {
        keyboard_1->addView(newKey(keyboardMap_2.at(0).at(i)));
    }
    for (int i = 0;i < keyboardMap_2.at(1).size();i++) {
        keyboard_2->addView(newKey(keyboardMap_2.at(1).at(i)));
    }
    for (int i = 0;i < keyboardMap_2.at(2).size();i++) {
        keyboard_3->addView(newKey(keyboardMap_2.at(2).at(i)));
    }
    for (int i = 0;i < keyboardMap_2.at(3).size();i++) {
        keyboard_4->addView(newKey(keyboardMap_2.at(3).at(i)));
    }
    keyboard_4->addView(newKey(1));
}

KInput::~KInput() {
    // delete anim;
}

std::string KInput::getInput() {
    return input->getText();
}

bool KInput::getcursorstatus() {
    return cursorstatus;
}

void KInput::setcursorstatus(bool status) {
    cursorstatus = status;
}

std::wstring KInput::stringToWstring(const std::string& str) {
    std::wstring wstr;
    wstr.reserve(str.size());

    for (char ch : str) {
        if (ch & 0x80) {
            // 多字节字符
            wchar_t wch = 0;
            int numBytes = 0;

            if ((ch & 0xE0) == 0xC0) {
                // 2字节字符
                wch = ch & 0x1F;
                numBytes = 1;
            } else if ((ch & 0xF0) == 0xE0) {
                // 3字节字符
                wch = ch & 0x0F;
                numBytes = 2;
            } else if ((ch & 0xF8) == 0xF0) {
                // 4字节字符
                wch = ch & 0x07;
                numBytes = 3;
            }

            for (int i = 0; i < numBytes; ++i) {
                ch = str[++i];
                wch = (wch << 6) | (ch & 0x3F);
            }

            wstr.push_back(wch);
        } else {
            // 单字节字符
            wstr.push_back(static_cast<wchar_t>(ch));
        }
    }
    return wstr;
}

std::string KInput::wstringToString(const std::wstring& wstr) {
    std::string str;
    str.reserve(wstr.size());

    for (wchar_t wch : wstr) {
        if (wch <= 0x7F) {
            // 单字节字符
            str.push_back(static_cast<char>(wch));
        } else if (wch <= 0x7FF) {
            // 2字节字符
            str.push_back(static_cast<char>(0xC0 | ((wch >> 6) & 0x1F)));
            str.push_back(static_cast<char>(0x80 | (wch & 0x3F)));
        } else if (wch <= 0xFFFF) {
            // 3字节字符
            str.push_back(static_cast<char>(0xE0 | ((wch >> 12) & 0x0F)));
            str.push_back(static_cast<char>(0x80 | ((wch >> 6) & 0x3F)));
            str.push_back(static_cast<char>(0x80 | (wch & 0x3F)));
        } else {
            // 4字节字符
            str.push_back(static_cast<char>(0xF0 | ((wch >> 18) & 0x07)));
            str.push_back(static_cast<char>(0x80 | ((wch >> 12) & 0x3F)));
            str.push_back(static_cast<char>(0x80 | ((wch >> 6) & 0x3F)));
            str.push_back(static_cast<char>(0x80 | (wch & 0x3F)));
        }
    }
    return str;
}

std::string KInput::getWord(const char* buffer, int count) {
    std::string words;
    int         wordCount = 0;
    const char* pos = buffer;
    const char* special_word[] = { "£", "·" };
    const int   special_word_count = 2;

    while (*pos && wordCount < count) {
        if (*pos > 0 && *pos <= 127) {
            wordCount++;
            words.append(1, *(pos++));
        } else {
            wordCount++;

            bool is_special = false;
            int  i, j, k;
            for (i = 0; i < special_word_count; i++) {
                for (j = 0, k = strlen(special_word[i]); j < k; j++) {
                    if (*(pos + j) != special_word[i][j]) {
                        break;
                    }
                    if (j + 1 == k) {
                        is_special = true;
                    }
                }
                if (is_special) {
                    break;
                }
            }
            if (i < special_word_count) {
                words.append(special_word[i]);
                pos += k;
            } else {
                // 默认3个字节
                words.append(1, *(pos++));
                words.append(1, *(pos++));
                words.append(1, *(pos++));
            }
        }
    }
    return words;
}