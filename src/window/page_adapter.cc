#include <page_adapter.h>
#include <homewindow.h>

MyPageAdapter::MyPageAdapter(){
    myData = modeData;
}

int MyPageAdapter::getCount(){
    return myData.size();
}
bool MyPageAdapter::isViewFromObject(View* view, void*object){ 
    return view==object;
}

void* MyPageAdapter::instantiateItem(ViewGroup* container, int position) {
    LOGD("instantiateItem positino=%d",position);

    View*itemview=new MyLinearLayout(position);

    itemview->setTag((void*)position); 

    container->addView(itemview);
    return itemview;
}


void MyPageAdapter::destroyItem(ViewGroup* container, int position,void* object){
    LOGD("destroyItem positino=%d",position);
    modeData.at(position).isChange=false;
    container->removeView((View*)object);
    delete (View*)object;
}

float MyPageAdapter::getPageWidth(int position){
    return 1.f;
}

void MyPageAdapter::finishUpdate(ViewGroup* container){

}

std::string MyPageAdapter::getPageTitle(int position){
    return myData.at(position).item;
}

void MyPageAdapter::notifyDataChanged(){
    myData = modeData;
    notifyDataSetChanged();
}