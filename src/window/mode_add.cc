#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

#include "mode.h"
#include <homewindow.h>

ModeAddWindow::ModeAddWindow(ModeAdapter* adapter, ModeWindow* container) : Window(0, 0, -1, -1) {
    fromSetting = true;
    setCache(container);
    TextView* view = buildView();
    view->setOnClickListener([this, adapter](View& view) mutable {
        adapter->addMode(this->getCache());
        LOGD("保存成功");
        this->close();
        }
    );
}

ModeAddWindow::ModeAddWindow(int wendu, int chushuiliang) :Window(0, 0, -1, -1) {
    fromSetting = false;
    setCache(wendu, chushuiliang);
    TextView* view = buildView();
    view->setOnClickListener([this](View& view) mutable {
        if (modeData.size() < 14) {
            modeData.push_back(modeCache);
            LOGD("保存成功");
            HomeWindow::ins()->notifyDataChanged();
            this->close();
        } else {
            LOGD("保存失败，模式总数已达上限！");
        }
        }
    );
}

void ModeAddWindow::setCache(ModeWindow* container) {
    modeView = container;
    modeCache.picture_2 = "@mipmap/pic_chushui_changwen";
    modeCache.picture = "@mipmap/pic_changwenshui";
    modeCache.item = "自定义";
    modeCache.wendu = 45;
    modeCache.chushuiliang = 350;
    modeCache.type = 1;
    LOGD("%s----%d----%d----%d", modeCache.item, modeCache.wendu, modeCache.chushuiliang, modeCache.type);
}

void ModeAddWindow::setCache(int wendu, int chushuiliang) {
    modeCache.picture_2 = "@mipmap/pic_chushui_changwen";
    modeCache.picture = "@mipmap/pic_changwenshui";
    modeCache.item = "自定义";
    modeCache.wendu = wendu;
    modeCache.chushuiliang = chushuiliang;
    modeCache.type = 1;
    LOGD("%s----%d----%d----%d", modeCache.item, modeCache.wendu, modeCache.chushuiliang, modeCache.type);
}

TextView* ModeAddWindow::buildView() {
    LayoutInflater::from(getContext())->inflate("@layout/mode_add", this);
    defaultSelect = 1;

    ImageView* add_img = (ImageView*)findViewById(z10::R::id::mode_add_img);
    add_img->setBackgroundResource(modeCache.picture);
    add_img->setOnClickListener([this, add_img](View& view) {
        if (defaultSelect == 6)defaultSelect = 0;
        add_img->setBackgroundResource(modeImgMap.at(defaultSelect));
        modeCache.picture = modeImgMap.at(defaultSelect);
        modeCache.picture_2 = modeImgMap2.at(defaultSelect);
        defaultSelect++;
        });

    TextView* tv = (TextView*)findViewById(z10::R::id::mode_add_name);
    tv->setText(modeCache.item);
    auto rename = std::bind(&ModeAddWindow::ModeRename, this, std::placeholders::_1);
    tv->setOnClickListener(rename);

    tv = (TextView*)findViewById(z10::R::id::mode_add_warm_text);
    tv->setText(std::to_string(modeCache.wendu) + "℃");
    tv = (TextView*)findViewById(z10::R::id::mode_add_water_text);
    tv->setText(std::to_string(modeCache.chushuiliang) + "mL");

    ImageView* home = (ImageView*)findViewById(z10::R::id::mode_add_home);

    if (fromSetting) {
        home->setOnClickListener([this](View& view)mutable {
            modeView->closesetting();
            this->close();
            });
    } else {
        home->setVisibility(View::GONE);
    }


    RelativeLayout* title_back = (RelativeLayout*)findViewById(z10::R::id::title_back);
    title_back->setOnClickListener([this](View& view)mutable {
        LOGD("窗口即将关闭");
        this->close();
        });


    // 温度
    ImageView* warmDown = (ImageView*)findViewById(z10::R::id::mode_add_warm_down);
    ImageView* warmUp = (ImageView*)findViewById(z10::R::id::mode_add_warm_up);
    tv = (TextView*)findViewById(z10::R::id::mode_add_warm_text);
    warmDown->setOnClickListener([this, tv, warmDown, warmUp](View& view)mutable {
        LOGD("温度减");
        modeCache.wendu -= 5;
        if (modeCache.wendu <= 45) {
            modeCache.wendu = 45;
            warmDown->setVisibility(View::INVISIBLE);
        }
        tv->setText(std::to_string(modeCache.wendu) + "℃");
        warmUp->setVisibility(View::VISIBLE);
        });
    warmUp->setOnClickListener([this, tv, warmUp, warmDown](View& view)mutable {
        LOGD("温度加");
        modeCache.wendu += 5;
        if (modeCache.wendu >= 100) {
            modeCache.wendu = 100;
            warmUp->setVisibility(View::INVISIBLE);
        }
        tv->setText(std::to_string(modeCache.wendu) + "℃");
        warmDown->setVisibility(View::VISIBLE);
        });

    // 水量
    ImageView* waterDown = (ImageView*)findViewById(z10::R::id::mode_add_water_down);
    ImageView* waterUp = (ImageView*)findViewById(z10::R::id::mode_add_water_up);
    tv = (TextView*)findViewById(z10::R::id::mode_add_water_text);
    waterDown->setOnClickListener([this, tv, waterDown, waterUp](View& view)mutable {
        LOGD("水量减");
        modeCache.chushuiliang -= 50;
        if (modeCache.chushuiliang <= 150) {
            modeCache.chushuiliang = 150;
            waterDown->setVisibility(View::INVISIBLE);
        }
        tv->setText(std::to_string(modeCache.chushuiliang) + "mL");
        waterUp->setVisibility(View::VISIBLE);
        });
    waterUp->setOnClickListener([this, tv, waterUp, waterDown](View& view)mutable {
        LOGD("水量加");
        modeCache.chushuiliang += 50;
        if (modeCache.chushuiliang >= 1000) {
            modeCache.chushuiliang = 1000;
            waterUp->setVisibility(View::INVISIBLE);
        }
        tv->setText(std::to_string(modeCache.chushuiliang) + "mL");
        waterDown->setVisibility(View::VISIBLE);
        });

    return (TextView*)findViewById(z10::R::id::mode_add_enter);
}

void ModeAddWindow::ModeRename(View& view) {
    KInput* rename = new KInput("", "请输入该自定义出水模式的名称", 6, "@mipmap/intput_bg_add");
    // KInput* rename = new KInput(modeCache.item,"请输入该自定义出水模式的名称", 6, "@mipmap/intput_bg_add");

    TextView* inputEnter = (TextView*)rename->findViewById(z10::R::id::input_enter);
    inputEnter->setOnClickListener([this, rename](View& view) {
        this->setCacheName(rename->getInput());
        rename->close();
        });
}

void ModeAddWindow::setCacheName(std::string name) {
    TextView* tv = (TextView*)findViewById(z10::R::id::mode_add_name);
    tv->setText(name);
    modeCache.item = name;
}

ModeAddWindow::~ModeAddWindow() {

}
