#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

#include "screenlight.h"

ScreenLightWindow::ScreenLightWindow() : Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/screenlight", this);

    auto FuncClickBack = std::bind(&ScreenLightWindow::onClickBackListener,this,std::placeholders::_1);

    findViewById(z10::R::id::linearLayout)->setOnClickListener(FuncClickBack);
    findViewById(z10::R::id::setting_screenLight_Box)->setOnClickListener(FuncClickBack);
    findViewById(z10::R::id::setting_screenLight_cancel)->setOnClickListener(FuncClickBack);
    findViewById(z10::R::id::setting_screenLight_enter)->setOnClickListener(FuncClickBack);

    ImageView *image_bright = (ImageView *)findViewById(z10::R::id::image_bright);
    image_bright->setLayerType(View::LAYER_TYPE_SOFTWARE);
    RotateDrawable *rotate = (RotateDrawable *)image_bright->getDrawable();

    mLightSeekbar = (SeekBar*)findViewById(z10::R::id::setting_seekbar);
    mLightSeekbar->setProgress(g_appData.light);

    image_bright->setAlpha(80.0/120.0);
    rotate->setLevel(7250 + 80*27.5);  
    mLightSeekbar->setOnSeekBarChangeListener([this,image_bright,rotate](AbsSeekBar& seek, int progress, bool fromuser) {
        LOGD("Light:%d", progress);
        float alpha = float(progress + 20)/120.f;
        image_bright->setAlpha(alpha);

        rotate->setLevel(7500 + float(progress -10)*2500.0 /90.0);                                         
    });
}

ScreenLightWindow::~ScreenLightWindow() {
}

void ScreenLightWindow::onClickBackListener(View&v){ 
    switch(v.getId()){
        case z10::R::id::setting_screenLight_Box:
            return;
        case z10::R::id::linearLayout:
        case z10::R::id::setting_screenLight_cancel:
            break;
        case z10::R::id::setting_screenLight_enter:
            g_appData.light = mLightSeekbar->getProgress();
            break;
    }
    this->close();
}
