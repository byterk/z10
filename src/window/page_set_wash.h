#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>

#include <data.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

class Page_set_wash:public Window{
private:
    NumberPicker *mPickeStarHour;
    NumberPicker *mPickeStarMin;
    NumberPicker *mPickeEndHour;
    NumberPicker *mPickeEndMin;

    void onClickBackListener(View&v);
public:
    Page_set_wash();
    ~Page_set_wash();
};

