#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>

#include <mode.h>
#include <data.h>
#include <arc_seekbar.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

#define PROGRESS_STAR_NUM 150
#define PROGRESS_ADD_NUM 50
#define PROGRESS_DIFF (PROGRESS_STAR_NUM - PROGRESS_ADD_NUM)/PROGRESS_ADD_NUM     // 出水量 progress 与 真实值的差值（如：150mL为最低值，应为（150-50）/50）

class MyLinearLayout:public LinearLayout{
public:
    MyLinearLayout(int position);
    ~MyLinearLayout();

    void outWaterBtnStatus();
    void resetPageData();   
    void outWaterDownCallback();                // 出水完成的回调函数
private:
    WATER myitem;
    int mposition;
        
    void initLinearPage();       //初始化页面
    void setLinearData();                           
    void onChangerListener(View &v, int progress);  //监听 滚动条事件
    void onClickViewListener(View&v);               // 监听点击事件

    void ChangeCslData(bool isAdd);                 //处理出水量变化 （增加、减少）
    void ChangeWdData(bool isAdd);                  //处理温度变化（增加、减少）
    
    void changeState();                         // 温度状态改变，进入自定义
    void changeTemCallback();                   // 温度改变的回调函数
    
    ArcSeekBar *mArcWendu;                      //温度   的 ArcSeekBar 控件，即 圆形 滑动条
    TextView   *mTextWendu;                     //温度   的 textView控件

    ArcSeekBar *mArcChushuiliang;               //出水量 的 ArcSeekBar 控件，即 圆形 滑动条
    TextView   *mTextChushuiliang;              //出水量 的 textView控件

    ImageView  *mImageLeft;                     //左边图片
    ImageView  *mImageRight;                    //右边图片（出水、出冰图标）
    TextView   *mPictureText;                         

    Button *mBtnFavorite;                       //收藏按钮
    Button *mBtnOutlet;
    bool isChange;
};