#include <popwindow_hint.h>

Popwindow_text_hint::Popwindow_text_hint(ViewGroup *parent,int func,std::string text,std::string enter,std::string cancel):mParentViewGroup(parent),mFunFlag(func){
    mViewGroup =  (ViewGroup *)LayoutInflater::from(mParentViewGroup->getContext())->inflate("@layout/popwindow_text_hint",nullptr);
    mParentViewGroup->addView(mViewGroup);

    mTextView  = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_hint_text);
    mBtnCancel = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_hint_cancel);
    mBtnEnter  = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_hint_enter);

    mTextView->setText(text);
    mBtnCancel->setText(cancel);
    mBtnEnter->setText(enter);
    
    mTextView->setLineHeight(45);

    Runnable mrunnable;
    mrunnable = [this](){
        HomeWindow::ins()->selectTab(); 
        mViewGroup->findViewById(z10::R::id::linearLayout)->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));  //点击 空白地方返回上一级
        mTextView->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));     //截取 文本的点击事件 
        mBtnCancel->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));
        mBtnEnter->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));

    };
    mViewGroup->postDelayed(mrunnable,500);
}

Popwindow_text_hint::Popwindow_text_hint(ViewGroup *parent, int func):mParentViewGroup(parent){
    mViewGroup =  (ViewGroup *)LayoutInflater::from(mParentViewGroup->getContext())->inflate("@layout/popwindow_text_hint",mParentViewGroup);
    LOGI("Popwindow_text_hint()");
    initState(func);
    mTextView   = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_hint_text);
    mBtnCancel = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_hint_cancel);
    mBtnEnter  = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_hint_enter);

    mTextView->setText(mText);
    mBtnCancel->setText(mCancel_Text);
    mBtnEnter->setText(mEnter_Text);
    
    mTextView->setLineHeight(45);

    Runnable mrunnable;
    mrunnable = [this](){
        mViewGroup->findViewById(z10::R::id::linearLayout)->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));  //点击 空白地方返回上一级
        mTextView->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));         //截取 文本的点击事件 
        mBtnCancel->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));   
        mBtnEnter->setOnClickListener(std::bind(&Popwindow_text_hint::onClickBackListener,this,std::placeholders::_1));
        HomeWindow::ins()->selectTab();
    };
    mViewGroup->postDelayed(mrunnable,500);
}

Popwindow_text_hint::~Popwindow_text_hint()
{
    delete mTextView;
    delete mBtnCancel;
    delete mBtnEnter;
}

void Popwindow_text_hint::initState(int func){
    if(func == INPUT_FUNC_COOL){
        if(g_appData.zhileng == PRO_STATE_COOL_NONE){           //如果制冷为开启
            mFunFlag = OPEN_FUNC_COOL;
            mText = std::string("开启制冷功能后，系统将开始制作冷水");
            mEnter_Text = std::string("开启制冷");
        }
        else if((g_appData.zhileng == PRO_STATE_COOL_ING || g_appData.zhileng == PRO_STATE_COOL_DONE) && g_appData.zhibing == PRO_STATE_ICE_NONE){       //如果正在制冷 或 制冷完成(没有开启制冰)
            mFunFlag = CLOSE_FUNC_COOL;
            mText = std::string("是否关闭制冷功能？");
            mEnter_Text = std::string("确认关闭");
        }
        else if((g_appData.zhileng == PRO_STATE_COOL_ING || g_appData.zhileng == PRO_STATE_COOL_DONE) && (g_appData.zhibing == PRO_STATE_ICE_DONE || g_appData.zhibing == PRO_STATE_ICE_ING)){      //如果正在制冷 或 制冷完成( 开启制冰了 )
            mFunFlag = CLOSE_FUNC_COOL_ICE;
            mText = std::string("是否关闭制冷功能？");
            mEnter_Text = std::string("确认关闭");
        }
    }
    else if(func == INPUT_FUNC_ICE){
        if(g_appData.zhileng == PRO_STATE_COOL_NONE && g_appData.zhibing == PRO_STATE_ICE_NONE){        //如果 制冷没有开启 和 制冰没有开启
            mFunFlag = OPEN_FUNC_COOL_ICE;
            mText = std::string("启用冰块模式需先打开制冷制冰功能");
            mEnter_Text = std::string("开启制冷制冰");
        }
        else if(g_appData.zhibing == PRO_STATE_ICE_NONE){                                               //如果 制冷有开启 和 制冰没有开启
            mFunFlag = OPEN_FUNC_ICE;
            mText = std::string("开启制冰功能后，系统将开始制作冰块");
            mEnter_Text = std::string("开启制冰");
        }
        else if(g_appData.zhibing == PRO_STATE_ICE_ING || g_appData.zhibing == PRO_STATE_ICE_DONE){     //如果 正在制冰 或 制冰完成
            mFunFlag = CLOSE_FUNC_ICE;
            mText = std::string("是否关闭制冰功能？");
            mEnter_Text = std::string("确认关闭");
        }
    }
    mCancel_Text = std::string("取消");
}

void Popwindow_text_hint::onClickBackListener(View&v){
    switch(v.getId()){
        case z10::R::id::popwindow_hint_text:
            return;
        case z10::R::id::popwindow_hint_cancel:
        case z10::R::id::linearLayout:
            break;
        case z10::R::id::popwindow_hint_enter:
            switch(mFunFlag){
                case OPEN_FUNC_COOL:
                    g_appData.zhileng = PRO_STATE_COOL_ING;         //设置为正在制冷 
                    g_objConnMgr->setCooling(CS_COOL);
                    break;
                case OPEN_FUNC_ICE:
                    g_appData.zhibing = PRO_STATE_ICE_ING;          //设置为正在制冰
                    g_objConnMgr->setCooling(CS_COOL_ICE);
                    break;
                case OPEN_FUNC_COOL_ICE:
                    g_appData.zhileng = PRO_STATE_COOL_ING;         //设置为正在制冷
                    g_appData.zhibing = PRO_STATE_ICE_ING;          //设置为正在制冰 
                    g_objConnMgr->setCooling(CS_COOL_ICE);
                    break;
                case CLOSE_FUNC_COOL:
                    g_appData.zhileng = PRO_STATE_COOL_NONE;        //设置为 不制冷
                    g_objConnMgr->setCooling(CS_NULL);
                    HomeWindow::ins()->CheckSelect();
                    break;
                case CLOSE_FUNC_ICE:
                    g_appData.zhibing = PRO_STATE_ICE_NONE;         //设置为 不制冰 (但是正在制冷)
                    g_objConnMgr->setCooling(CS_COOL);              
                    HomeWindow::ins()->CheckSelect();
                    break;
                case CLOSE_FUNC_COOL_ICE:
                    g_appData.zhileng = PRO_STATE_COOL_NONE;         //设置为 不制冷
                    g_appData.zhibing = PRO_STATE_ICE_NONE;          //设置为 不制冰
                    g_objConnMgr->setCooling(CS_NULL);      
                    HomeWindow::ins()->CheckSelect(); 
                    break;
            }
            g_objConnMgr->send2MCU();
            Activity_header::ins()->upHeader();             //更新主页的图标
            LOGI("send data : 制冷 = %s 制冰 = %s",g_appData.zhileng == PRO_STATE_COOL_NONE ? "false":"true" 
                                                 ,g_appData.zhibing == PRO_STATE_ICE_NONE ? "false":"true");
            
            break;
    }
    mParentViewGroup->removeView(mViewGroup);
}


/////////////////////////////////////////////////////////////////////////////////
Popwindow_tip_hint::Popwindow_tip_hint(ViewGroup *parent, int func):mParentViewGroup(parent){
    mViewGroup = (ViewGroup *)LayoutInflater::from(mParentViewGroup->getContext())->inflate("@layout/popwindow_tip_hint",mParentViewGroup);
    TextView *text1 = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_tip_text);
    TextView *text2 = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_tip_text2);
    TextView *text3 = (TextView *)mViewGroup->findViewById(z10::R::id::popwindow_tip_text3);

    if(func == INPUT_FUNC_ICE_ING){
        text1->setText(std::string("系统制冰中，请稍后"));
        text2->setText(std::string("制冰状态灯变成"));
        text3->setText(std::string("后，可出冰"));
    }
    else if(func == INPUT_FUNC_COOL_ING){
        text1->setText(std::string("系统制冷中，请稍后"));
        text2->setText(std::string("制冷状态灯变成"));
        text3->setText(std::string("后，可出冷水"));
    }

    Runnable mrunnable;
    mrunnable = [this](){
        mViewGroup->findViewById(z10::R::id::linearLayout)->setOnClickListener(std::bind(&Popwindow_tip_hint::onClickBackListener,this,std::placeholders::_1));     //点击空白返回
        mViewGroup->findViewById(z10::R::id::popwindow_tip_hint_enter)->setOnClickListener(std::bind(&Popwindow_tip_hint::onClickBackListener,this,std::placeholders::_1));
        mViewGroup->findViewById(z10::R::id::popwindow_Box)->setOnClickListener(std::bind(&Popwindow_tip_hint::onClickBackListener,this,std::placeholders::_1));    //截取 文本的点击事件  

        HomeWindow::ins()->selectTab();
    };
    mViewGroup->postDelayed(mrunnable,500);
}

Popwindow_tip_hint::~Popwindow_tip_hint()
{
}

void Popwindow_tip_hint::onClickBackListener(View&v){
    switch(v.getId()){
        case z10::R::id::popwindow_tip_hint_enter:
        case z10::R::id::linearLayout:
            mParentViewGroup->removeView(mViewGroup);
            break;
    }
    
}