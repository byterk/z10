#pragma once
#include <widget/cdwindow.h>
#include<widget/textview.h>
#include<widget/gridlayout.h>

#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

// typedef struct{
//     std::string type;
// }keyboardMap;


class KInput :public Window {
    LinearLayout* keyboard_1;
    LinearLayout* keyboard_2;
    LinearLayout* keyboard_3;
    LinearLayout* keyboard_4;
    TextView* input;
    TextView* inputDes;

    std::string textDes;  //描述
    int textMaxNum;       //输入内容的上限
    std::string inputBg;  //页面背景

    bool cursorstatus;

    bool enType;
    bool fuhao;
    std::vector<std::vector<std::string>> keyboardMap = {
        {"q","w","e","r","t","y","u","i","o","p"},
        {"a","s","d","f","g","h","j","k","l"},
        {"z","x","c","v","b","n","m"},
        {"Q","W","E","R","T","Y","U","I","O","P"},
        {"A","S","D","F","G","H","J","K","L"},
        {"Z","X","C","V","B","N","M"}
    };
    std::vector<std::vector<std::string>> keyboardMap_2 = {
        {"-","/",":",";","(",")","$","&","@","\""},
        {"[","]","{","}","#","%","^","*","+","="},
        {"_","\\","|","~","<",">","€","£","¥","·"},
        {".",",","?","!","'"},
    };
    std::vector<int> imgTag;
private:
    View* newKey(std::string text);
    View* newKey(int type);
    void onKeyClick(View& view);
    void onKeyImgClick(View& view);

    void listenClick();

    void setKey();
    void setKey_num();
    void setKey_yingwen(bool type);
    void setKey_fuhao();
protected:
public:
    KInput(std::string iText, std::string description = "描述", int maxNum = 6, std::string background = "@mipmap/bg");
    ~KInput();

    Runnable cursor_blink;

    std::string getInput();
    bool getcursorstatus();
    void setcursorstatus(bool status);

    std::wstring stringToWstring(const std::string& str);
    std::string wstringToString(const std::wstring& wstr);
    std::string getWord(const char* buffer, int count);
};
