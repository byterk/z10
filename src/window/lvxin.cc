#include <cdroid.h>
#include <R.h>
#include <cdlog.h>

#include <homewindow.h>
#include "lvxin.h"

LvXinWindow::LvXinWindow() : Window(0, 0, -1, -1) {
    LayoutInflater::from(getContext())->inflate("@layout/lvxin", this);
    // g_objConnMgr->mLvxinCallback=std::bin

    arc_HPCC       = (ArcSeekBar*)findViewById(z10::R::id::lvXin_arc_HPCC);
    arc_HPCC_text  = (TextView*)findViewById(z10::R::id::lvXin_arc_HPCC_text);
    arc_RO         = (ArcSeekBar*)findViewById(z10::R::id::lvXin_arc_RO);
    arc_RO_text    = (TextView*)findViewById(z10::R::id::lvXin_arc_RO_text);
    totalWaterText = (TextView*)findViewById(z10::R::id::total_water);
    prueWaterText  = (TextView*)findViewById(z10::R::id::prue_water_tds);
    cleanWaterText = (TextView*)findViewById(z10::R::id::clean_water_tds);
    
    arc_HPCC_text->setText(std::to_string(g_appData.PPLvxinPercen));
    arc_HPCC->setProgress(std::round(3.6 * g_appData.PPLvxinPercen));
    arc_RO_text->setText(std::to_string(g_appData.ROLvxinPercen));
    arc_RO->setProgress(std::round(3.6 * g_appData.ROLvxinPercen));
    totalWaterText->setText(std::to_string(g_appData.totalWater));
    prueWaterText->setText(std::to_string(g_appData.prueWaterTDS));
    cleanWaterText->setText(std::to_string(g_appData.cleanWaterTDS));

    auto click_func = std::bind(&LvXinWindow::buttonClick, this, std::placeholders::_1);

    TextView* reset = (TextView*)findViewById(z10::R::id::lvXin_reSet_HPCC);
    reset->setOnClickListener(click_func);

    reset = (TextView*)findViewById(z10::R::id::lvXin_reSet_RO);
    reset->setOnClickListener(click_func);

    RelativeLayout* title_back = (RelativeLayout*)findViewById(z10::R::id::title_back);
    title_back->setOnClickListener(click_func);

    TextView* paiShui = (TextView*)findViewById(z10::R::id::lvXin_paiShui);
    paiShui->setOnClickListener(click_func);
    TextView* congXi = (TextView*)findViewById(z10::R::id::lvXin_congXi);
    congXi->setOnClickListener(click_func);

    mRunnable=[this](){
        int HPCC = std::stoi(arc_HPCC_text->getText());
        int RO   = std::stoi(arc_RO_text->getText());

        if((resetPPStatus == RESET_PP_ING) && (HPCC>0)){
            arc_HPCC_text->setText(std::to_string(HPCC-1));
            arc_HPCC->setProgress(std::round(3.6 * (HPCC-1)));
        }else if(HPCC == 0 ){
            resetPPStatus = RESET_NONE;
        }

        if((resetROStatus == RESET_RO_ING)&& (RO>0)){
            arc_RO_text->setText(std::to_string(RO-1));
            arc_RO->setProgress(std::round(3.6 * (RO-1)));
        }else if(RO == 0){
            resetROStatus = RESET_NONE;
        }
        
        if((resetROStatus == RESET_NONE)&&(resetPPStatus == RESET_NONE)){
            removeCallbacks(mRunnable);
            return;
        }
        postDelayed(mRunnable,20);
        LOG(VERBOSE) << "Runnable...";  
    };
}

LvXinWindow::~LvXinWindow() {
    removeCallbacks(mRunnable);
}

void LvXinWindow::buttonClick(View& view) {
    switch (view.getId()) {
    case z10::R::id::title_back:
        this->close();
        break;

    case z10::R::id::lvXin_reSet_HPCC: {
        resetLvxin(RESET_PP_ING);
        break;}

    case z10::R::id::lvXin_reSet_RO: {
        resetLvxin(RESET_RO_ING);
        break;}

    case z10::R::id::lvXin_paiShui:
        LOGD("开启排水");
        // 暂未作处理
        break;

    case z10::R::id::lvXin_congXi:
        LOGD("开启冲洗");
        // 暂未作处理
        break;

    default:
        break;
    }
}

void LvXinWindow::resetLvxin(int value){
    if((value == RESET_PP_ING)&&(resetPPStatus != RESET_PP_ING)){
        removeCallbacks(mRunnable);
        resetPPStatus = RESET_PP_ING;
        g_appData.isResetPP=true;
        g_objConnMgr->setPPReset(true);
        g_objConnMgr->send2MCU();
        postDelayed(mRunnable,20);
        LOG(VERBOSE) << "send data : PP滤芯复位(HPCC滤芯)";  
    }else if((value == RESET_RO_ING)&&(resetPPStatus != RESET_RO_ING)){
        removeCallbacks(mRunnable);
        resetROStatus = RESET_RO_ING;
        g_appData.isResetRO=true;
        g_objConnMgr->setROReset(true);
        g_objConnMgr->send2MCU();
        postDelayed(mRunnable,20);
        LOG(VERBOSE) << "send data : RO滤芯复位";  
    }
}
