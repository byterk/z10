#include <page_item.h>
#include <homewindow.h>

MyLinearLayout::MyLinearLayout(int position):LinearLayout(0,0,-1,-1){
    modeData.at(position).myins = this;
    
    myitem = modeData.at(position);
    mposition = position;
    myitem.isChange = false;
    initLinearPage();
}

MyLinearLayout::~MyLinearLayout(){
    if(modeData.size() > mposition){
        modeData.at(mposition).isChange = false;
        modeData.at(mposition).myins = nullptr;
    }
    delete mImageLeft;
    delete mBtnFavorite;
    delete mPictureText;
    if(myitem.type == MODE_TYPE_COOL_WATER)
        HomeWindow::ins()->removeAppCallback(SC_COOL_WATER_TEM);
    else if(myitem.type == MODE_TYPE_SIMPLE_WATER)
        HomeWindow::ins()->removeAppCallback(SC_INWATER_TEM);
}

void MyLinearLayout::initLinearPage(){    
    LayoutInflater::from(getContext())->inflate("@layout/page_item",this);
    this->setBackgroundColor(0x00ffffff);

    mImageLeft = (ImageView *)findViewById(z10::R::id::page_picture);             //左边 图片
    mImageLeft->setImageResource(myitem.picture);                                 //设置图片

    mPictureText = (TextView *)findViewById(z10::R::id::page_picture_text);
    mPictureText->setText(myitem.item);

    FrameLayout *linear=(FrameLayout *)findViewById(z10::R::id::framelayout_page);
    
    mBtnOutlet = (Button *)findViewById(z10::R::id::page_picture2_btn);            //出水、出冰按钮
    mBtnOutlet->setOnClickListener(std::bind(&MyLinearLayout::onClickViewListener,this,std::placeholders::_1));     

    mBtnFavorite = (Button *)findViewById(z10::R::id::page_keep);
    mBtnFavorite->setEnabled(false);

    mImageRight = (ImageView *)findViewById(z10::R::id::page_picture2);                      //图片2   右边出冰 或者 出水
    mImageRight->setOnClickListener(std::bind(&MyLinearLayout::onClickViewListener,this,std::placeholders::_1));                         //出水、出冰 图标按钮
    if( myitem.type == MODE_TYPE_ICE)
    {
        LOG(VERBOSE) << "进入了冰块页面";
        LayoutInflater::from(getContext())->inflate("@layout/page_item_bingkuai",linear);
        
        mBtnOutlet->setText("出冰");
        mImageRight->setImageResource("@mipmap/icon_ice_chubing");
        
    }
    else if(myitem.type == MODE_TYPE_COOL_WATER){
        LOGD("进入了冷水页面");
        LayoutInflater::from(getContext())->inflate("@layout/page_item_lengshui",linear);
        setLinearData();
        mArcWendu->setProgress(myitem.wendu);
        mTextWendu->setText(std::to_string(g_appData.iceWaterTem));
        mArcWendu->setMin(myitem.wendu-1);
        mArcWendu->setMax(myitem.wendu);
        HomeWindow::ins()->addAppCallback(SC_COOL_WATER_TEM,std::bind(&MyLinearLayout::changeTemCallback,this));
    }
    else if(myitem.type == MODE_TYPE_SIMPLE_WATER){
        LOG(VERBOSE) << "进入了常温水页面";
        LayoutInflater::from(getContext())->inflate("@layout/page_item_lengshui",linear);
        setLinearData();
        mArcWendu->setProgress(myitem.wendu);
        mTextWendu->setText(std::to_string(g_appData.realWaterTem));
        mArcWendu->setMin(myitem.wendu-1);
        mArcWendu->setMax(myitem.wendu);
        HomeWindow::ins()->addAppCallback(SC_INWATER_TEM,std::bind(&MyLinearLayout::changeTemCallback,this));
    }
    else {
        LOG(VERBOSE) << "进入了其他页面";
        LayoutInflater::from(getContext())->inflate("@layout/page_item_zidingyi",linear);
        setLinearData();

        mArcWendu->setProgress(myitem.wendu);
        mTextWendu->setText(std::to_string(myitem.wendu));
        mArcWendu->setRange(45,98);

        findViewById(z10::R::id::wendu_reduce)->setOnClickListener(std::bind(&MyLinearLayout::onClickViewListener,this,std::placeholders::_1));                   //监听 温度 按钮 （-）
        findViewById(z10::R::id::wendu_add)->setOnClickListener(std::bind(&MyLinearLayout::onClickViewListener,this,std::placeholders::_1));                         //监听 温度 按钮 （+）

        mArcWendu->setOnChangeListener(std::bind(&MyLinearLayout::onChangerListener,this,std::placeholders::_1,std::placeholders::_2)); //监听 温度 的变化
    }
    outWaterBtnStatus();
}

void MyLinearLayout::setLinearData(){
    mArcWendu  = (ArcSeekBar *)findViewById(z10::R::id::arc_wendu);                    //温度   的 ArcSeekBar 控件，即 圆形 滑动条
    mTextWendu = (TextView   *)findViewById(z10::R::id::txt_wendu);                      //温度   的 textView控件
    mArcChushuiliang  = (ArcSeekBar *)findViewById(z10::R::id::arc_chushuiliang);      //出水量 的 ArcSeekBar 控件，即 圆形 滑动条
    mTextChushuiliang = (TextView   *)findViewById(z10::R::id::txt_chushuiliang);        //出水量 的 textView控件

    mArcChushuiliang->setProgress((myitem.chushuiliang/PROGRESS_ADD_NUM)-PROGRESS_DIFF);
    if((myitem.chushuiliang/PROGRESS_ADD_NUM) >= 20-PROGRESS_DIFF)  mTextChushuiliang->setText(std::string("持续"));  // 持续出水
    else   mTextChushuiliang->setText(std::to_string((myitem.chushuiliang/PROGRESS_ADD_NUM)*PROGRESS_ADD_NUM));

    mArcChushuiliang->setRange(1,20-PROGRESS_DIFF);
    mArcChushuiliang->setMax(20-PROGRESS_DIFF);
    mArcChushuiliang->setMin(1);
  
    mImageRight->setImageResource("@mipmap/icon_chushui");

    mArcChushuiliang->setOnChangeListener(std::bind(&MyLinearLayout::onChangerListener,this,std::placeholders::_1,std::placeholders::_2));  //监听 出水量 的变化
                              
    findViewById(z10::R::id::chushuiliang_reduce)->setOnClickListener(std::bind(&MyLinearLayout::onClickViewListener,this,std::placeholders::_1));                      //监听 出水量 按钮 （-）
    findViewById(z10::R::id::chushuiliang_add)->setOnClickListener(std::bind(&MyLinearLayout::onClickViewListener,this,std::placeholders::_1));                            //监听 出水量 按钮 （+）
    
    if(myitem.type == MODE_TYPE_COOL_WATER){
        ((TextView *)findViewById(z10::R::id::min_wendu))->setText(std::string("6 ℃"));
        ((TextView *)findViewById(z10::R::id::max_wendu))->setText(std::string("12 ℃"));
    }
    mBtnFavorite->setOnClickListener([this](View&v){
        ModeAddWindow *w = new ModeAddWindow(std::stoi(mTextWendu->getText()),(mArcChushuiliang->getProgress()+PROGRESS_DIFF)*PROGRESS_ADD_NUM);
    });
}

// ArcSeekBar 滑动条改变 事件
void MyLinearLayout::onChangerListener(View &v, int progress){      
    switch(v.getId()){
        case z10::R::id::arc_wendu:
            mTextWendu->setText(std::to_string(progress));
            LOGD("change wendu progress  = %d ",progress);
            v.getParent()->requestDisallowInterceptTouchEvent(true);
            if(!myitem.isChange)
                changeState();
            break;
        case z10::R::id::arc_chushuiliang: 
            if(progress >= 20-PROGRESS_DIFF)  mTextChushuiliang->setText(std::string("持续"));  // 持续出水
            else                              mTextChushuiliang->setText(std::to_string((progress+PROGRESS_DIFF)*PROGRESS_ADD_NUM));      //滚动条每次变化 50ml
            LOGD("change chushuiliang progress  = %d ",(progress+PROGRESS_DIFF)*PROGRESS_ADD_NUM);            //滚动条每次变化 50ml
            v.getParent()->requestDisallowInterceptTouchEvent(true);
            if(!myitem.isChange){
                modeData.at(mposition).isChange = true;
            }
            break;
    }
}

// 处理控件 点击事件
void MyLinearLayout::onClickViewListener(View&v){
    LOGD("onClickViewListener() ");
    switch(v.getId()){
        case z10::R::id::wendu_reduce:   
            ChangeWdData(false);    
            break;
        case z10::R::id::wendu_add:
            ChangeWdData(true);          
            break;
        case z10::R::id::chushuiliang_reduce:       //点击减少出水量   点击按钮每次减少 50ml
            ChangeCslData(false);
            break;
        case z10::R::id::chushuiliang_add:          //点击增加出水量   点击按钮每次增加 50ml
            ChangeCslData(true);
            break;
        case z10::R::id::page_picture2_btn:         //点击出水按钮、图片
        case z10::R::id::page_picture2:{
            if(myitem.type == MODE_TYPE_ICE){
                g_objConnMgr->setWaterAction(WA_NULL);                      // 设置 出水 动作为空
                g_objConnMgr->setIceAction(true);                           // 设置 出冰 
                g_appData.iceStatus=true;
                LOGI("send data : 设置为出冰");
            }else{
                if(myitem.type == MODE_TYPE_COOL_WATER){
                    g_appData.outWaterStatus = WA_COLL_WATER;
                    g_objConnMgr->setWaterAction(WA_COLL_WATER);            // 设置出水为冰水
                }else if(myitem.type == MODE_TYPE_SIMPLE_WATER){
                    g_appData.outWaterStatus = WA_HOT_WATER;
                    g_appData.outWaterTem = 25;
                    g_objConnMgr->setWaterAction(WA_HOT_WATER);             // 设置出水为常温水
                    g_objConnMgr->setWaterTem(25);                          // 设置温度 25度则为出水常温水                                   
                }else {
                    g_appData.outWaterStatus = WA_HOT_WATER;
                    g_appData.outWaterTem = mArcWendu->getProgress();
                    g_objConnMgr->setWaterAction(WA_HOT_WATER);             // 设置出水为热水
                    g_objConnMgr->setWaterTem(mArcWendu->getProgress());    // 设置温度    
                }
                g_appData.showStatusTDS = PRO_STATE_TDS_SHOW;
                Activity_header::ins()->changeState(PRO_STATE_TDS_SHOW); 
                g_objConnMgr->setWaterYield((mArcChushuiliang->getProgress()+PROGRESS_DIFF)*PROGRESS_ADD_NUM);  //出水量
                LOGI("send data : 设置为%s    出水量=%d",myitem.type == MODE_TYPE_COOL_WATER ? "出冰水":"出热水",(mArcChushuiliang->getProgress()+PROGRESS_DIFF)*PROGRESS_ADD_NUM);                   
            }
            g_objConnMgr->send2MCU();
            mBtnOutlet->setText("暂停");
            mBtnOutlet->setEnabled(false);
            mImageRight->setEnabled(false);
            HomeWindow::ins()->removeAppCallback(SC_OUT_WATER_COMPLETE);
            HomeWindow::ins()->addAppCallback(SC_OUT_WATER_COMPLETE,std::bind(&MyLinearLayout::outWaterDownCallback,this));
            modeData.at(mposition).isChange = true;
            break;
        }
    }
}

// 处理出水量按钮点击
void MyLinearLayout::ChangeCslData(bool isAdd){
    int CSL=mArcChushuiliang->getProgress();
    if(isAdd){
        if((CSL+1) >= 20-PROGRESS_DIFF)
            CSL = 20-PROGRESS_DIFF;
        else
            CSL+=1;
    }else{
        if((CSL-1) <= 1)
            CSL = 1;
        else
            CSL-=1;
    }
    if(CSL >= 20-PROGRESS_DIFF) mTextChushuiliang->setText(std::string("持续"));  // 持续出水
    else                        mTextChushuiliang->setText(std::to_string((CSL+PROGRESS_DIFF)*PROGRESS_ADD_NUM)); 
    mArcChushuiliang->setProgress(CSL);
}

// 处理温度按钮点击
void MyLinearLayout::ChangeWdData(bool isAdd){
    int Wendu=mArcWendu->getProgress();
    if(isAdd){
        if(Wendu >=95 && Wendu <= 98)
            Wendu = 98;
        else if((Wendu+5) >= 95)
            Wendu = 95;
        else 
            Wendu += 5;
    }else{
        if(Wendu >95 && Wendu <= 98)
            Wendu = 95;
        else if( (Wendu-5) <= 45)
            Wendu = 45;
        else 
            Wendu -= 5;
    }
    if(!myitem.isChange)
        changeState();
    mArcWendu->setProgress(Wendu);
    mTextWendu->setText(std::to_string(Wendu));
}

//温度状态改变，进入自定义
void MyLinearLayout::changeState(){        
    mPictureText->setText(std::string("自定义"));
    mBtnFavorite->setEnabled(true);
    myitem.isChange = true;
    modeData.at(mposition).isChange = true;
}

void MyLinearLayout::outWaterBtnStatus(){
    if(g_appData.outWaterStatus == WA_NULL){
        mBtnOutlet->setEnabled(true);
        mImageRight->setEnabled(true);
        if(myitem.type == MODE_TYPE_ICE) mBtnOutlet->setText("出冰");
        else mBtnOutlet->setText("出水");
    }else{
        mBtnOutlet->setText("暂停");
        mBtnOutlet->setEnabled(false);
        mImageRight->setEnabled(false);
    }
    
}

//如果状态有改变，说明上次进入了自定义，需要重新初始化
void MyLinearLayout::resetPageData(){
    mPictureText->setText(myitem.item);
    mBtnFavorite->setEnabled(false);

    mArcWendu->setProgress(myitem.wendu);

    if(myitem.type == MODE_TYPE_COOL_WATER) mTextWendu->setText(std::to_string(g_appData.iceWaterTem));
    else if(myitem.type == MODE_TYPE_SIMPLE_WATER) mTextWendu->setText(std::to_string(g_appData.realWaterTem));
    else mTextWendu->setText(std::to_string(myitem.wendu));
    
    mArcChushuiliang->setProgress((myitem.chushuiliang/PROGRESS_ADD_NUM)-PROGRESS_DIFF);
    mTextChushuiliang->setText(std::to_string(myitem.chushuiliang));

    myitem.isChange = false;
    modeData.at(mposition).isChange = false;

    LOGD("resetPageData() ");
}

void MyLinearLayout::changeTemCallback(){
    LOGI("changeTemCallback()");
    if(myitem.type == MODE_TYPE_COOL_WATER) mTextWendu->setText(std::to_string(g_appData.iceWaterTem));
    else if(myitem.type == MODE_TYPE_SIMPLE_WATER) mTextWendu->setText(std::to_string(g_appData.realWaterTem));
    else mTextWendu->setText(std::to_string(myitem.wendu));
}

void MyLinearLayout::outWaterDownCallback(){
    LOGI("outWaterDownCallback()");
    if(myitem.type == MODE_TYPE_ICE){
        mBtnOutlet->setText("出冰");
        mBtnOutlet->setEnabled(true);
        mImageRight->setEnabled(true);
    }else{
        mBtnOutlet->setText("出水");
        mBtnOutlet->setEnabled(true);
        mImageRight->setEnabled(true);
        Activity_header::ins()->changeState(PRO_STATE_TDS_HIDE);          
    }
}
