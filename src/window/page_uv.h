#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>

#include <data.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

class Page_uv:public Window{
private:
    NumberPicker *mPickeStarHour;
    NumberPicker *mPickeStarMin;
    NumberPicker *mPickeEndHour;
    NumberPicker *mPickeEndMin;

    CheckBox *mCheckBoxKeepOpen;
    CheckBox *mCheckBoxTimeOpen;
    CheckBox *mCheckBoxInitativeOpen;

    Button *mBtnNowShajun;

    void onClickBackListener(View&v);
    void onCheckedChangeListener(CompoundButton&view,bool isChecked);
    void onNumPickerValueChange(NumberPicker &npx, int index, int value);
public:
    Page_uv();
    ~Page_uv();
};

