#include <activity_header.h>

ViewGroup *Activity_header::parent = nullptr;

Activity_header::Activity_header(){
    memset(&mg_appData,0,sizeof(mg_appData));
}

Activity_header::~Activity_header(){
}

void Activity_header::init(){
    mTongsuo = (ImageView *)parent->findViewById(z10::R::id::icon_tongsuo);               //童锁                                     
    auto func=std::bind(&Activity_header::onButtonClick,this,std::placeholders::_1);
    mTongsuo->setOnClickListener(func);

    mLvxin = (ImageView *)parent->findViewById(z10::R::id::icon_lvxin);                   //滤芯
    mLvxin->setOnClickListener(func);

    mUVshajun = (TextView *)parent->findViewById(z10::R::id::text_top_uv);                  //uv杀菌
    mUVshajun->setOnClickListener(func);

    mTDS = (TextView *)parent->findViewById(z10::R::id::text_top_tds);                //tds    （不能点击）

    mZhaoming = (ImageView *)parent->findViewById(z10::R::id::icon_zhaoming);             //照明
    mZhaoming->setOnClickListener(func);

    mZhileng = (TextView *)parent->findViewById(z10::R::id::text_top_zhileng);            //制冷
    mZhileng->setOnClickListener(func);

    mZhibing = (TextView *)parent->findViewById(z10::R::id::text_top_zhibing);            //制冰
    mZhibing->setOnClickListener(func);
 
    mTime = (TextView *)parent->findViewById(z10::R::id::text_top_time);                  //时间
    mTime->setOnClickListener(func);
    
    mNet = (ImageView *)parent->findViewById(z10::R::id::icon_wifi);                     //wifi
    mNet->setOnClickListener(func);

    mSetup = (ImageView *)parent->findViewById(z10::R::id::icon_setup);                   //设置
    mSetup->setOnClickListener(func);

    mRunnable = [this](){               //更新时间 1s 更新一次
        std::string showTime = getTimeStr();
        mTime->setText(showTime);
        mTime->postDelayed(mRunnable,1000);
    };
    mTime->post(mRunnable);
    upHeader();
}

//监听控件
void Activity_header::onButtonClick(View&v){
    switch(v.getId())
    {
        case z10::R::id::icon_tongsuo:{
            LOGD("case z10::R::id::icon_tongsuo");
            if(g_appData.childLocked == PRO_STATE_TONGSUO){
                Page_Unlock *wd = Page_Unlock::ins();
            }    
            break;
        }
            
        case z10::R::id::icon_lvxin:{
            LvXinWindow *w = new LvXinWindow;
            break;
        }
            
        case z10::R::id::text_top_uv:{
            Page_uv *w = new Page_uv;
            break;
        }
            
        case z10::R::id::icon_zhaoming:{
            if(g_appData.zhaoming == PRO_STATE_ZHAOMING_CLOSE)    g_appData.zhaoming = PRO_STATE_ZHAOMING;
            else    g_appData.zhaoming = PRO_STATE_ZHAOMING_CLOSE;
            changeState((State)g_appData.zhaoming);
            break;
        }
            
        case z10::R::id::text_top_zhileng:{     // 制冷
            Popwindow_text_hint *window = new Popwindow_text_hint(parent,INPUT_FUNC_COOL);     //弹窗 
            break;
        }      
            
        case z10::R::id::text_top_zhibing:{     // 制冰
            Popwindow_text_hint *window = new Popwindow_text_hint(parent,INPUT_FUNC_ICE);      //弹窗
            break;
        }    

        case z10::R::id::text_top_time:{
            Page_set_time *wt = new Page_set_time;
            break;
        }

        case z10::R::id::icon_wifi:{
            NetWorkWindow *wt = new NetWorkWindow;
            break;
        }
            
        case z10::R::id::icon_setup:{
            Page_setting *sw = new Page_setting;
            break;
        }
            
    }
}

//更新状态
void Activity_header::upHeader(){
    State cState;
    while(1){
        cState = checkState();
        if( cState!= PRO_STATE_NONE){
            changeState(cState);
            continue;
        }else{
            break;
        }
    }
}

//检查状态
State Activity_header::checkState(){
    if(mg_appData.childLocked != g_appData.childLocked){
        mg_appData.childLocked = g_appData.childLocked;
        return (State)g_appData.childLocked;
    }
    if(mg_appData.lvxin != g_appData.lvxin){
        mg_appData.lvxin = g_appData.lvxin;
        return (State)g_appData.lvxin;
    }
    if(mg_appData.UVshajun != g_appData.UVshajun){
        mg_appData.UVshajun = g_appData.UVshajun;
        return (State)g_appData.UVshajun;
    }
    if(mg_appData.showStatusTDS != g_appData.showStatusTDS){
        mg_appData.showStatusTDS = g_appData.showStatusTDS;
        return (State)g_appData.showStatusTDS;
    }
    if(mg_appData.zhaoming != g_appData.zhaoming){
        mg_appData.zhaoming = g_appData.zhaoming;
        return (State)g_appData.zhaoming;
    }
    if(mg_appData.zhileng != g_appData.zhileng){
        mg_appData.zhileng = g_appData.zhileng;
        return (State)g_appData.zhileng;
    }
    if(mg_appData.zhibing != g_appData.zhibing){
        mg_appData.zhibing = g_appData.zhibing;
        return (State)g_appData.zhibing;
    }
    if(mg_appData.netStatus != g_appData.netStatus){
        mg_appData.netStatus = g_appData.netStatus;
        return (State)g_appData.netStatus;
    }
    return PRO_STATE_NONE;
}

void Activity_header::changeState(State state){
    switch(state){
        case PRO_STATE_TONGSUO:{
            mTongsuo->setImageResource("@mipmap/icon_tongsuo");
            break;
        }case PRO_STATE_TONGSUO_CLOSE:{
            mTongsuo->setImageResource("@mipmap/icon_tongsuo1");
            break;
        }case PRO_STATE_LVXIN_NORMAL:{
            mLvxin->setImageResource("@mipmap/icon_lvxin");
            break;
        }case PRO_STATE_LVXIN_SOON:{
            mLvxin->setImageResource("@mipmap/icon_lvxin_yellow"); 
            break;
        }case PRO_STATE_LVXIN_EXPIRED:{
            mLvxin->setImageResource("@mipmap/icon_lvxin_red");
            break;
        }case PRO_STATE_UV_RESERVATION:{
            if(g_appData.isUVshajun) mUVshajun->setTextColor(0xFF64ADF4);
            else                     mUVshajun->setTextColor(0xFFE7E7E7);
            break;
        }case PRO_STATE_UV_NONE:
        case PRO_STATE_UV_USER:{
            g_appData.isUVshajun = false;
            mUVshajun->setTextColor(0xFFE7E7E7);
            break;
        }case PRO_STATE_UV_KEEP_ENABLED:{
            g_appData.isUVshajun = true;
            mUVshajun->setTextColor(0xFF64ADF4);
            break;
        }
        case PRO_STATE_TDS_SHOW:{
            mTDS->setTextColor(0xFF64ADF4);
            mTDS->setText(std::string("TDS: ") + std::to_string(g_appData.cleanWaterTDS));
            break;
        }case PRO_STATE_TDS_HIDE:{
            mTDS->setTextColor(0x00FFFFFF);
            mTDS->setText(" "); 
            break;
        }case PRO_STATE_ZHAOMING:{
            mZhaoming->setImageResource("@mipmap/icon_zhaomingopen");
            break;
        }case PRO_STATE_ZHAOMING_CLOSE:{
            mZhaoming->setImageResource("@mipmap/icon_zhaoming");
            break;
        }case PRO_STATE_COOL_NONE:{
            mZhileng->setCompoundDrawablesWithIntrinsicBounds("@mipmap/icon_close","nullptr","nullptr","nullptr");
            break;
        }case PRO_STATE_COOL_ING:{
            mZhileng->setCompoundDrawablesWithIntrinsicBounds("@mipmap/icon_ing","nullptr","nullptr","nullptr");
            break;
        }case PRO_STATE_COOL_DONE:{
            mZhileng->setCompoundDrawablesWithIntrinsicBounds("@mipmap/icon_wancheng","nullptr","nullptr","nullptr");
            break;
        }case PRO_STATE_ICE_NONE:{
            mZhibing->setCompoundDrawablesWithIntrinsicBounds("@mipmap/icon_close","nullptr","nullptr","nullptr");
            break;
        }case PRO_STATE_ICE_ING:{
            mZhibing->setCompoundDrawablesWithIntrinsicBounds("@mipmap/icon_ing","nullptr","nullptr","nullptr");
            break;
        }case PRO_STATE_ICE_DONE:{
            mZhibing->setCompoundDrawablesWithIntrinsicBounds("@mipmap/icon_wancheng","nullptr","nullptr","nullptr");
            break;
        }case PRO_STATE_NET_NONE:{
            mNet->setImageLevel(5);
            break;
        }case PRO_STATE_NET_ONE:{
            mNet->setImageLevel(1);
            break;
        }case PRO_STATE_NET_TWO:{
            mNet->setImageLevel(2);
            break;
        }case PRO_STATE_NET_THREE:{
            mNet->setImageLevel(3);
            break;
        }case PRO_STATE_NET_FULL:{
            mNet->setImageLevel(4);
            break;
        }
    }         
}