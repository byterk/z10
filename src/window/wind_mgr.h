﻿
#ifndef __wind_mgr_h__
#define __wind_mgr_h__

#include <project.h>
#include <comm_view.h>

// 业务窗口管理
class CWindMgr
{
public:
    static CWindMgr *ins()
    {
        static CWindMgr ins;
        return &ins;
    }

    // 显示窗口
    void showWind(emWindowID ID);
    // 状态通知
    void statusNotify(int value = 0);

public:
    void showMain();
    bool exists(emWindowID ID);
    bool existsClose(emWindowID ID);
    void closeWindow(int windID);
    void closeOther(emWindowID Id);
    void closeOtherEx(const std::set<int> &Id);
    void check();    

protected:
    CWindMgr(){}
};

extern CWindMgr *g_objWindMgr;

#endif
