#pragma once
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <widget/cdwindow.h>

#include <data.h>

class ModeWindow : public Window {
    Window* settingView;
private:
protected:
    void ModeWindowClick(View&);
public:
    ModeWindow(Window* container);
    ~ModeWindow();
    void closesetting();
};

// 新增页面
class ModeAddWindow :public Window {
    ModeWindow* modeView;
    bool fromSetting;
    WATER modeCache;
    
    std::vector<std::string> modeImgMap = { "@mipmap/pic_lengshui","@mipmap/pic_changwenshui","@mipmap/pic_feishui","@mipmap/pic_paonai","@mipmap/pic_fengmishui","@mipmap/pic_lvcha" };
    std::vector<std::string> modeImgMap2 = { "@mipmap/pic_chushui_lengshui","@mipmap/pic_chushui_changwen","@mipmap/pic_chushui_feishui","@mipmap/pic_chushui_paonai","@mipmap/pic_chushui_fengmi","@mipmap/pic_chushui_lvcha" };
    int defaultSelect;
private:
    void setCache(ModeWindow* container);
    void setCache(int wendu, int chushuiliang);
protected:
public:
    ModeAddWindow(class ModeAdapter* adapter, ModeWindow* container);
    ModeAddWindow(int wendu, int chushuiliang);
    ~ModeAddWindow();
    TextView* buildView();
    WATER getCache() { return modeCache; };

    void ModeRename(View& view);
    void setCacheName(std::string name);
};

// 编辑页面
class ModeEditWindow :public Window {
    ModeWindow* modeView;
    WATER modeCache;

    std::vector<std::string> modeImgMap = { "@mipmap/pic_lengshui","@mipmap/pic_changwenshui","@mipmap/pic_feishui","@mipmap/pic_paonai","@mipmap/pic_fengmishui","@mipmap/pic_lvcha" };
    std::vector<std::string> modeImgMap2 = { "@mipmap/pic_chushui_lengshui","@mipmap/pic_chushui_changwen","@mipmap/pic_chushui_feishui","@mipmap/pic_chushui_paonai","@mipmap/pic_chushui_fengmi","@mipmap/pic_chushui_lvcha" };
    int defaultSelect;
private:
    void ViewClick(View& view);
    void setCache(int position);
protected:
public:
    ModeEditWindow(class ModeAdapter* adapter, int position, ModeWindow* container);
    ~ModeEditWindow();

    void ModeRename(View& view);
    void setCacheName(std::string name);
};

// 删除模式弹窗
class delModeWindow :public Window {
public:
    delModeWindow();
    ~delModeWindow() { };
};

// 
class ModeAdapter : public Adapter {
    ModeWindow* thisContainer;
    int skipdata = 1;

protected:
    void DelPop(int position);
public:
    ModeAdapter(ModeWindow* container);
    void* getItem(int position) const override { return nullptr; };
    int getCount() const override { return modeData.size() - skipdata; };
    View* getView(int position, View* convertView, ViewGroup* parent) override;
    void changedata(int location, int dir);
    void delMode(int location);
    void addMode(WATER cache);
    void updateMode(int location, WATER cache);
    void buildedit(int position);
    void buildadd();
    ModeWindow* findcontainer() { return thisContainer; };
};
