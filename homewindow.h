#pragma once
#include <widget/cdwindow.h>

#include <widget/relativelayout.h>
#include <widget/numberpicker.h>
#include <widget/textview.h>
#include <widget/linearlayout.h>
#include <core/inputeventsource.h>

#include <conn_mgr.h>
#include <comm_func.h>
#include <data.h>
#include <page_set_wash.h>
#include <mode.h>
#include <lvxin.h>
#include <screenlight.h>
#include <network.h>
#include <page_setting.h>
#include <page_unlock.h>
#include <screen_saver.h>
#include <activity_header.h>
#include <popwindow_hint.h>
#include <page_set_time.h>
#include <page_uv.h>
#include <page_adapter.h>
#include <page_item.h>
#include <arc_seekbar.h>
#include <input.h>
#include <cdroid.h>
#include <R.h>
#include <cdlog.h>
#include <clocale>

typedef std::function<void()> app_callback;

class HomeWindow:public Window{
private:
    std::map<appStatusChange, app_callback> mAppCallback; // 程序回调函数

    ViewPager   *mViewPager;
    ViewGroup   *mViewGroup;
    TabLayout   *mTabLayout;
    MyPageAdapter *mAdapter;

    int mPrevPagePosition = 2;          //初始化 为 第三个页面

    Runnable mRunnable;

    bool isNotify = false;
    bool isCanSelect = true;

    HomeWindow();
    ~HomeWindow();
protected:
    void onTabSelectedListener(TabLayout::Tab& tab);
    bool checkIsNeedPop(int positon);
    View* resetTabCustom(int type,int positon);
    void screenSaver(bool bEnabled);
    void resetPagePosition();
public:
    static HomeWindow *ins() {
        static HomeWindow ins;
        return &ins;
    }
    void notifyDataChanged();
    void selectTab();
    void CheckSelect();
    
    void AppCallback(appStatusChange status);
    void removeAppCallback(appStatusChange status);                 // 删除回调
    void addAppCallback(appStatusChange status, app_callback callback); // 添加回调
    void appStatusNotify(appStatusChange status,int value = 0);     // 程序状态更新
};

